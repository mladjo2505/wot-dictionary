var words = [
{
  word:        "a",
  translation: "o",
  type:        "adj."
},
{
  word:        "able",
  translation: "folyt",
  type:        "adj."
},
{
  word:        "about",
  translation: "tiel",
  type:        "adv. & prep."
},
{
  word:        "account",
  translation: "norvenne",
  type:        "n."
},
{
  word:        "acid",
  translation: "ghar",
  type:        "n."
},
{
  word:        "across",
  translation: "gaen",
  type:        "prep."
},
{
  word:        "act",
  translation: "bunok",
  type:        "v."
},
{
  word:        "addition",
  translation: "einto",
  type:        "n."
},
{
  word:        "adjustment",
  translation: "faerstin",
  type:        "n."
},
{
  word:        "after",
  translation: "widon",
  type:        "adv., prep. & conj."
},
{
  word:        "again",
  translation: "nye",
  type:        "adv."
},
{
  word:        "against",
  translation: "bat",
  type:        "prep."
},
{
  word:        "agony",
  translation: "dha",
  type:        "n."
},
{
  word:        "agreement",
  translation: "o’vin",
  type:        "n."
},
{
  word:        "air",
  translation: "raia",
  type:        "n."
},
{
  word:        "all",
  translation: "aes",
  type:        "n."
},
{
  word:        "all-powerful, the",
  translation: "ghraem",
  type:        "n."
},
{
  word:        "almost",
  translation: "sast",
  type:        "adv."
},
{
  word:        "always",
  translation: "hei",
  type:        "adv."
},
{
  word:        "am",
  translation: "misain (insistent; emphatic)",
  type:        "v."
},
{
  word:        "am no(t)",
  translation: "isainde (insistent; emphatic)",
  type:        "v. neg."
},
{
  word:        "among",
  translation: "tom",
  type:        "prep."
},
{
  word:        "amount",
  translation: "abakran",
  type:        "n."
},
{
  word:        "amusement",
  translation: "seel",
  type:        "n."
},
{
  word:        "and",
  translation: "e",
  type:        "conj."
},
{
  word:        "angle",
  translation: "lato",
  type:        "n."
},
{
  word:        "angry",
  translation: "cavastu",
  type:        "adj."
},
{
  word:        "anguish",
  translation: "dha",
  type:        "n."
},
{
  word:        "animal",
  translation: "ubunto",
  type:        "n."
},
{
  word:        "annihilation",
  translation: "bhan",
  type:        "n."
},
{
  word:        "anniversary marriage celebration (Saldaean)",
  translation: "shanna’har",
  type:        "n."
},
{
  word:        "another",
  translation: "sovya",
  type:        "adj."
},
{
  word:        "answer",
  translation: "conagh",
  type:        "n."
},
{
  word:        "ant",
  translation: "pizar",
  type:        "n."
},
{
  word:        "anxiety",
  translation: "cuande (stress-induced condition experienced as chest pains)",
  type:        "n."
},
{
  word:        "any",
  translation: "shak",
  type:        "pron., adj. & adv."
},
{
  word:        "any other",
  translation: "sovya",
  type:        "adj."
},
{
  word:        "apparatus",
  translation: "jobei",
  type:        "n."
},
{
  word:        "apple",
  translation: "melimo",
  type:        "n."
},
{
  word:        "approval",
  translation: "modan",
  type:        "n."
},
{
  word:        "arch",
  translation: "onadh",
  type:        "n."
},
{
  word:        "argument",
  translation: "zheshi",
  type:        "n."
},
{
  word:        "arm",
  translation: "bazam",
  type:        "n."
},
{
  word:        "army",
  translation: "dumki",
  type:        "n."
},
{
  word:        "arrow",
  translation: "vasen",
  type:        "n."
},
{
  word:        "arrow of time",
  translation: "vasen’cierto",
  type:        "n."
},
{
  word:        "art",
  translation: "beatha",
  type:        "n."
},
{
  word:        "as",
  translation: "sene",
  type:        "adv."
},
{
  word:        "ask",
  translation: "devor",
  type:        "v."
},
{
  word:        "asked, what was",
  translation: "devoriska",
  type:        "rel. pron."
},
{
  word:        "at",
  translation: "thaz",
  type:        "prep."
},
{
  word:        "attack",
  translation: "baijan",
  type:        "n."
},
{
  word:        "attempt",
  translation: "gomaen",
  type:        "n. & v."
},
{
  word:        "attention",
  translation: "sahlan",
  type:        "n."
},
{
  word:        "attraction",
  translation: "amotath",
  type:        "n."
},
{
  word:        "aunt",
  translation: "shaendi",
  type:        "n."
},
{
  word:        "authority",
  translation: "rahtsi",
  type:        "n."
},
{
  word:        "automatic",
  translation: "n’baid",
  type:        "adj."
},
{
  word:        "awake",
  translation: "aagret",
  type:        "adj."
},
{
  word:        "baby",
  translation: "bopo",
  type:        "n."
},
{
  word:        "back",
  translation: "rakh",
  type:        "n., adj. & adv."
},
{
  word:        "bad",
  translation: "begoud",
  type:        "adj."
},
{
  word:        "bag",
  translation: "sich",
  type:        "n."
},
{
  word:        "balance",
  translation: "rieht",
  type:        "n."
},
{
  word:        "ball",
  translation: "dhub",
  type:        "n."
},
{
  word:        "band",
  translation: "samid",
  type:        "v."
},
{
  word:        "band/brotherhood/group",
  translation: "shen",
  type:        "n."
},
{
  word:        "Band of the Red Hand",
  translation: "Shen an Calhar",
  type:        "n."
},
{
  word:        "banner, small",
  translation: "con",
  type:        "n."
},
{
  word:        "bar/barrier",
  translation: "vadin",
  type:        "n."
},
{
  word:        "base",
  translation: "nupar (as in bottom or support)",
  type:        "n."
},
{
  word:        "basin",
  translation: "dal",
  type:        "n."
},
{
  word:        "basket",
  translation: "vhool",
  type:        "n."
},
{
  word:        "bath",
  translation: "badan",
  type:        "n."
},
{
  word:        "battle",
  translation: "dai; gai (n.)",
  type:        "n., v. & adj."
},
{
  word:        "battle, key",
  translation: "gai’don (one that will decide the war)",
  type:        "n."
},
{
  word:        "battle blood",
  translation: "daishar (meaning, glory)",
  type:        "n."
},
{
  word:        "battle, brother to/of",
  translation: "gaidin (Aes Sedai word for Warders)",
  type:        "n."
},
{
  word:        "Battle Lord",
  translation: "Dai Shan (title for Lan)",
  type:        "n."
},
{
  word:        "battle person",
  translation: "algai",
  type:        "n."
},
{
  word:        "battle-related",
  translation: "dhai",
  type:        "adj."
},
{
  word:        "battle, those sworn to peace in",
  translation: "gai’shain (Aiel term)",
  type:        "n."
},
{
  word:        "beautiful",
  translation: "n’am",
  type:        "adj."
},
{
  word:        "beauty, female ideal of",
  translation: "boan",
  type:        "n."
},
{
  word:        "beauty, male ideal of",
  translation: "botay",
  type:        "n."
},
{
  word:        "because",
  translation: "po",
  type:        "conj."
},
{
  word:        "bed",
  translation: "desu",
  type:        "n."
},
{
  word:        "bee",
  translation: "souk",
  type:        "n."
},
{
  word:        "beet",
  translation: "birok",
  type:        "n."
},
{
  word:        "before",
  translation: "ailen",
  type:        "conj., prep. & adj."
},
{
  word:        "behavior",
  translation: "soovri",
  type:        "n."
},
{
  word:        "beldam",
  translation: "drova",
  type:        "n."
},
{
  word:        "beldam",
  translation: "drovja",
  type:        "possessive"
},
{
  word:        "belief",
  translation: "astai",
  type:        "n."
},
{
  word:        "bell",
  translation: "kloye",
  type:        "n."
},
{
  word:        "belonging to",
  translation: "d (implied ownership or inferior position)",
  type:        "prep."
},
{
  word:        "beloved",
  translation: "lan",
  type:        "adj."
},
{
  word:        "bent",
  translation: "slagh",
  type:        "adj."
},
{
  word:        "berry",
  translation: "rimbai",
  type:        "n."
},
{
  word:        "betray",
  translation: "ishar",
  type:        "v."
},
{
  word:        "betrayal",
  translation: "ishavid",
  type:        "n."
},
{
  word:        "betrayer",
  translation: "isham",
  type:        "n."
},
{
  word:        "betrayer of hope",
  translation: "ishamael (name of a Forsaken)",
  type:        "n."
},
{
  word:        "between",
  translation: "scrup",
  type:        "adv. & prep."
},
{
  word:        "bird",
  translation: "kriko",
  type:        "n."
},
{
  word:        "birth",
  translation: "syndon",
  type:        "n."
},
{
  word:        "bit (past tense)",
  translation: "roedane",
  type:        "v."
},
{
  word:        "bite",
  translation: "roedna",
  type:        "v."
},
{
  word:        "bitter",
  translation: "chaki",
  type:        "adj."
},
{
  word:        "black",
  translation: "doon",
  type:        "n. & adj."
},
{
  word:        "Black Eyes",
  translation: "Seia Doon (Aiel warrior society)",
  type:        "n."
},
{
  word:        "Black Wind",
  translation: "Machin Shin (literally, ‘journey of destruction’)",
  type:        "n."
},
{
  word:        "blade",
  translation: "mandarb (name of Lan’s stallion); manshima",
  type:        "n."
},
{
  word:        "blade, related to",
  translation: "man",
  type:        "adj."
},
{
  word:        "blade, small",
  translation: "nai (knife-like)",
  type:        "n."
},
{
  word:        "blinder",
  translation: "samma",
  type:        "n."
},
{
  word:        "blood/bloodline (meaning descent or heritage)",
  translation: "shar (pl. is shari)",
  type:        "n."
},
{
  word:        "blood of battles",
  translation: "daishar (meaning, glory)",
  type:        "n."
},
{
  word:        "blow",
  translation: "khoop",
  type:        "v."
},
{
  word:        "blue",
  translation: "ascar; –era (suffix)",
  type:        "n. & adj."
},
{
  word:        "blue-eye",
  translation: "seiera (a flower in Baerlon; name of Min’s mare)",
  type:        "n."
},
{
  word:        "boar-horse",
  translation: "s’redit (used by Seanchan)",
  type:        "n."
},
{
  word:        "boar-like animal",
  translation: "capar (from Aiel Waste)",
  type:        "n."
},
{
  word:        "board",
  translation: "kontar",
  type:        "n."
},
{
  word:        "boat",
  translation: "nausig",
  type:        "n."
},
{
  word:        "body",
  translation: "chinnar",
  type:        "n."
},
{
  word:        "boiling",
  translation: "merwon",
  type:        "adj."
},
{
  word:        "bollocks",
  translation: "tsag (obsenity uttered by Sammael)",
  type:        "interjection"
},
{
  word:        "bone",
  translation: "khadi",
  type:        "n."
},
{
  word:        "book",
  translation: "blagh",
  type:        "n."
},
{
  word:        "boot(s)",
  translation: "poulam",
  type:        "n."
},
{
  word:        "bottle",
  translation: "tsatsi",
  type:        "n."
},
{
  word:        "bowl",
  translation: "dal",
  type:        "n."
},
{
  word:        "box(es)",
  translation: "bah(a)",
  type:        "n."
},
{
  word:        "boy",
  translation: "yuntar",
  type:        "n."
},
{
  word:        "brain",
  translation: "sysyn",
  type:        "n."
},
{
  word:        "brake",
  translation: "tsinas",
  type:        "n. & v."
},
{
  word:        "branch",
  translation: "warat",
  type:        "n."
},
{
  word:        "brass",
  translation: "farhota",
  type:        "n."
},
{
  word:        "bread",
  translation: "bachri",
  type:        "n."
},
{
  word:        "breath",
  translation: "chati",
  type:        "n."
},
{
  word:        "brick",
  translation: "yedcost",
  type:        "n."
},
{
  word:        "bridge",
  translation: "spotsu",
  type:        "n."
},
{
  word:        "bright",
  translation: "sheikar",
  type:        "adj."
},
{
  word:        "bringers of",
  translation: "sheen",
  type:        "n."
},
{
  word:        "Bringers of Annihilation",
  translation: "Bhan’sheen (a Trolloc band)",
  type:        "n."
},
{
  word:        "broken",
  translation: "yugol",
  type:        "adj."
},
{
  word:        "brother",
  translation: "alantin; din (singular and plural)",
  type:        "n."
},
{
  word:        "Brothers of the Eagle",
  translation: "Far Aldazar Din (an Aiel warrior society)",
  type:        "n."
},
{
  word:        "brother to/of battle",
  translation: "gaidin (Aes Sedai word for Warders)",
  type:        "n."
},
{
  word:        "Brotherhood",
  translation: "Ko’bal (a Trolloc band; literally, circle of one)",
  type:        "n."
},
{
  word:        "brotherhood/band/group",
  translation: "shen",
  type:        "n."
},
{
  word:        "Brotherless, the",
  translation: "Mera’din (used by Aiel)",
  type:        "n."
},
{
  word:        "brown",
  translation: "deebo",
  type:        "n. & adj."
},
{
  word:        "brush",
  translation: "jhabal",
  type:        "n."
},
{
  word:        "Brutes of Venom",
  translation: "Ghar’ghael (a Trolloc band)",
  type:        "n."
},
{
  word:        "bucket",
  translation: "hutsah",
  type:        "n."
},
{
  word:        "builders",
  translation: "wansho (Shienaran term for the Ogier)",
  type:        "n."
},
{
  word:        "building",
  translation: "bhardo",
  type:        "n."
},
{
  word:        "building material",
  translation: "cueran (from Age of Legends)",
  type:        "n."
},
{
  word:        "bulb",
  translation: "tumerest",
  type:        "n."
},
{
  word:        "burn",
  translation: "jalat",
  type:        "n. & v."
},
{
  word:        "burst",
  translation: "doozhi",
  type:        "v."
},
{
  word:        "business",
  translation: "pantae",
  type:        "n."
},
{
  word:        "but",
  translation: "no",
  type:        "conj."
},
{
  word:        "butter",
  translation: "maspil",
  type:        "n."
},
{
  word:        "button",
  translation: "sobel",
  type:        "n."
},
{
  word:        "by",
  translation: "dyu",
  type:        "adv. & prep."
},
{
  word:        "cabbage",
  translation: "soudhov",
  type:        "n."
},
{
  word:        "cake",
  translation: "qaato",
  type:        "n."
},
{
  word:        "call",
  translation: "aven",
  type:        "v."
},
{
  word:        "canvas",
  translation: "azafi",
  type:        "n."
},
{
  word:        "card",
  translation: "wuseta",
  type:        "n."
},
{
  word:        "care",
  translation: "dinya",
  type:        "v."
},
{
  word:        "carriage",
  translation: "yeel",
  type:        "n."
},
{
  word:        "carry",
  translation: "soende",
  type:        "v."
},
{
  word:        "cart",
  translation: "kuruta",
  type:        "n."
},
{
  word:        "cat",
  translation: "miou",
  type:        "n."
},
{
  word:        "cause",
  translation: "hadzi",
  type:        "v."
},
{
  word:        "cavalry",
  translation: "caba’drin",
  type:        "n."
},
{
  word:        "certain",
  translation: "zialin",
  type:        "adj."
},
{
  word:        "chain",
  translation: "ketvar",
  type:        "n."
},
{
  word:        "chalk",
  translation: "vezo",
  type:        "n."
},
{
  word:        "chamberlain (male)",
  translation: "shambayan (Borderlands term)",
  type:        "n."
},
{
  word:        "chance",
  translation: "dane; variant is diane",
  type:        "n."
},
{
  word:        "change",
  translation: "gadou",
  type:        "v."
},
{
  word:        "chatalaine (female)",
  translation: "shatayan (Borderlands term)",
  type:        "n."
},
{
  word:        "cheap",
  translation: "yaso",
  type:        "adj."
},
{
  word:        "cheese",
  translation: "brynza",
  type:        "n."
},
{
  word:        "chest",
  translation: "gozai",
  type:        "n."
},
{
  word:        "chief",
  translation: "car",
  type:        "n."
},
{
  word:        "chief of chiefs",
  translation: "car’a’carn (capitalized when Aiel use the title for Dragon Reborn)",
  type:        "n."
},
{
  word:        "chiefs",
  translation: "carn",
  type:        "n."
},
{
  word:        "chin",
  translation: "ghazh",
  type:        "n."
},
{
  word:        "chopsticks",
  translation: "sursa (used in Tanchico)",
  type:        "n."
},
{
  word:        "chora tree",
  translation: "Avendesora; Avendoraldera is an offshoot found outside the Waste",
  type:        "n."
},
{
  word:        "Chosen Ones",
  translation: "Da’concion (Seanchan term)",
  type:        "n."
},
{
  word:        "circle",
  translation: "bal",
  type:        "n."
},
{
  word:        "Circle of One",
  translation: "Ko’bal (a Trolloc band; meaning, brotherhood)",
  type:        "n."
},
{
  word:        "civilization",
  translation: "aes",
  type:        "n."
},
{
  word:        "claw",
  translation: "cha",
  type:        "n."
},
{
  word:        "claw",
  translation: "chalot",
  type:        "v."
},
{
  word:        "clean",
  translation: "tahni",
  type:        "adj."
},
{
  word:        "clear",
  translation: "udiya",
  type:        "adj."
},
{
  word:        "clock",
  translation: "dali",
  type:        "n."
},
{
  word:        "cloth",
  translation: "cadi",
  type:        "n."
},
{
  word:        "clothes",
  translation: "cadin",
  type:        "n."
},
{
  word:        "clothes, working",
  translation: "cadin’sor (worn by Aiel)",
  type:        "n."
},
{
  word:        "cloud(s)",
  translation: "dhakdi",
  type:        "n."
},
{
  word:        "coal",
  translation: "borz",
  type:        "n."
},
{
  word:        "coat",
  translation: "thorat",
  type:        "n."
},
{
  word:        "coffee",
  translation: "kaf (from Seanchan)",
  type:        "n."
},
{
  word:        "cold",
  translation: "zanda",
  type:        "n. & adj."
},
{
  word:        "collar",
  translation: "potsa",
  type:        "n."
},
{
  word:        "color",
  translation: "keymar",
  type:        "n."
},
{
  word:        "comb",
  translation: "shodet",
  type:        "n."
},
{
  word:        "come (do come)",
  translation: "ca’lyet; lyet",
  type:        "v."
},
{
  word:        "comfort",
  translation: "holubi",
  type:        "n."
},
{
  word:        "committee",
  translation: "yabedin",
  type:        "n."
},
{
  word:        "common",
  translation: "yabbeth",
  type:        "adj."
},
{
  word:        "company",
  translation: "bhadi",
  type:        "n."
},
{
  word:        "comparison",
  translation: "ohimat",
  type:        "n."
},
{
  word:        "competition",
  translation: "yaanaho",
  type:        "n."
},
{
  word:        "complete",
  translation: "zaffi",
  type:        "v."
},
{
  word:        "complex",
  translation: "dae",
  type:        "adj."
},
{
  word:        "complex game",
  translation: "dae’mar",
  type:        "n."
},
{
  word:        "concubine",
  translation: "asa (in Seanchan)",
  type:        "n."
},
{
  word:        "condition",
  translation: "shaff",
  type:        "n."
},
{
  word:        "connection",
  translation: "wabunen",
  type:        "n."
},
{
  word:        "conscious",
  translation: "bighar",
  type:        "adj."
},
{
  word:        "container",
  translation: "cour",
  type:        "n."
},
{
  word:        "control",
  translation: "mat",
  type:        "v."
},
{
  word:        "cook",
  translation: "rennen",
  type:        "n."
},
{
  word:        "cook",
  translation: "renni",
  type:        "v."
},
{
  word:        "copper",
  translation: "staba",
  type:        "n. & adj."
},
{
  word:        "copy",
  translation: "toulat",
  type:        "n."
},
{
  word:        "cord",
  translation: "dao",
  type:        "n."
},
{
  word:        "cork",
  translation: "fringfran",
  type:        "n."
},
{
  word:        "corn",
  translation: "zemai (from Aiel Waste)",
  type:        "n."
},
{
  word:        "cotton",
  translation: "algode (plant fiber from Aiel Waste)",
  type:        "n. & adj."
},
{
  word:        "cough",
  translation: "anouge",
  type:        "v."
},
{
  word:        "country",
  translation: "nyala",
  type:        "n."
},
{
  word:        "cover",
  translation: "wapro",
  type:        "v."
},
{
  word:        "cow",
  translation: "dudhi",
  type:        "n."
},
{
  word:        "coward, base or low",
  translation: "parano",
  type:        "n. & adj."
},
{
  word:        "crack",
  translation: "bairnu",
  type:        "v."
},
{
  word:        "credit",
  translation: "cheghar",
  type:        "n."
},
{
  word:        "crime",
  translation: "daarlot",
  type:        "n."
},
{
  word:        "cruel",
  translation: "vrang",
  type:        "adj."
},
{
  word:        "crush",
  translation: "maromi",
  type:        "v."
},
{
  word:        "cry",
  translation: "gheuth",
  type:        "v."
},
{
  word:        "cup",
  translation: "fintan",
  type:        "n."
},
{
  word:        "current, as in a river",
  translation: "ahenila",
  type:        "n."
},
{
  word:        "curtain",
  translation: "arahar",
  type:        "n."
},
{
  word:        "curve",
  translation: "ghal",
  type:        "v."
},
{
  word:        "cushion",
  translation: "nahodil",
  type:        "n."
},
{
  word:        "cut",
  translation: "nob",
  type:        "v."
},
{
  word:        "cutter",
  translation: "nor",
  type:        "n."
},
{
  word:        "dagger",
  translation: "gar; nai",
  type:        "n."
},
{
  word:        "daisy",
  translation: "mageen",
  type:        "n."
},
{
  word:        "damage",
  translation: "yohini",
  type:        "v."
},
{
  word:        "dance, type of",
  translation: "tiganza (Tinker dance); sara (Saldaean dance performed by women)",
  type:        "n."
},
{
  word:        "dancer(s)",
  translation: "hama (implies stately grace and fluidity)",
  type:        "n."
},
{
  word:        "dancer from Age of Legends",
  translation: "daien",
  type:        "n."
},
{
  word:        "danger",
  translation: "orobar",
  type:        "n."
},
{
  word:        "dark (as in pitch-darkness; indication of evil or wrong)",
  translation: "shaidar",
  type:        "n. & adj."
},
{
  word:        "dark, very",
  translation: "doon",
  type:        "n. & adj."
},
{
  word:        "Dark One (name)",
  translation: "Shai’tan",
  type:        "n."
},
{
  word:        "Darkfriend",
  translation: "Atha’an Shadar (literally, Darkfriend)",
  type:        "n."
},
{
  word:        "darkness, total",
  translation: "zamon",
  type:        "n."
},
{
  word:        "daughter",
  translation: "la",
  type:        "n."
},
{
  word:        "daughter of the night",
  translation: "lanfear (name of a Forsaken)",
  type:        "n."
},
{
  word:        "dawn",
  translation: "rahien",
  type:        "n."
},
{
  word:        "Dawn Runners",
  translation: "Rahien Sorei (Aiel warrior society)",
  type:        "n."
},
{
  word:        "day",
  translation: "nag",
  type:        "n."
},
{
  word:        "dead, the",
  translation: "ayend",
  type:        "n."
},
{
  word:        "dear",
  translation: "aada",
  type:        "adj."
},
{
  word:        "death",
  translation: "mordero",
  type:        "adj."
},
{
  word:        "debt",
  translation: "potadi",
  type:        "n."
},
{
  word:        "decision",
  translation: "watari",
  type:        "n."
},
{
  word:        "dedicated",
  translation: "aiel",
  type:        "n. & adj."
},
{
  word:        "deep",
  translation: "nush",
  type:        "adj."
},
{
  word:        "degree",
  translation: "zarin",
  type:        "n."
},
{
  word:        "delicate",
  translation: "binti",
  type:        "adj."
},
{
  word:        "dependent",
  translation: "diband",
  type:        "adj."
},
{
  word:        "derived from",
  translation: "dera",
  type:        "suffix"
},
{
  word:        "design",
  translation: "hienisus",
  type:        "n."
},
{
  word:        "desire",
  translation: "belo",
  type:        "v."
},
{
  word:        "desire to have",
  translation: "be’lal (the Envious, name of a Forsaken)",
  type:        "n."
},
{
  word:        "despised",
  translation: "tsang",
  type:        "adj."
},
{
  word:        "despised one",
  translation: "da’tsang (used by Aiel)",
  type:        "n."
},
{
  word:        "destined",
  translation: "maral",
  type:        "adj."
},
{
  word:        "destroyer",
  translation: "samma",
  type:        "n."
},
{
  word:        "destroyer of hope",
  translation: "sammael (name of a Forsaken)",
  type:        "n."
},
{
  word:        "destruction",
  translation: "machin",
  type:        "n."
},
{
  word:        "detail",
  translation: "platto",
  type:        "n."
},
{
  word:        "determined",
  translation: "cierto",
  type:        "adj."
},
{
  word:        "devastation",
  translation: "knotai",
  type:        "n."
},
{
  word:        "development",
  translation: "kramayage",
  type:        "n."
},
{
  word:        "devoted one",
  translation: "ba’asa",
  type:        "n."
},
{
  word:        "dice",
  translation: "dovie’andi",
  type:        "n."
},
{
  word:        "different",
  translation: "shitak",
  type:        "adj."
},
{
  word:        "digestion",
  translation: "taberan",
  type:        "n."
},
{
  word:        "direction",
  translation: "topito",
  type:        "n."
},
{
  word:        "dirty",
  translation: "mitris",
  type:        "adj."
},
{
  word:        "discovery",
  translation: "kaarash",
  type:        "n."
},
{
  word:        "discussion",
  translation: "korero",
  type:        "n."
},
{
  word:        "disease",
  translation: "spillon",
  type:        "n."
},
{
  word:        "disgust",
  translation: "agaroum",
  type:        "n."
},
{
  word:        "distance",
  translation: "beratam",
  type:        "n."
},
{
  word:        "distribution",
  translation: "nemhage",
  type:        "n."
},
{
  word:        "division",
  translation: "uldatein",
  type:        "n."
},
{
  word:        "do",
  translation: "ca (used as an intenfier; e.g., “I do come”)",
  type:        "v. aux."
},
{
  word:        "dog",
  translation: "shae",
  type:        "n."
},
{
  word:        "dogs",
  translation: "shae’en",
  type:        "n."
},
{
  word:        "Doom Pit",
  translation: "Shayol Ghul",
  type:        "n."
},
{
  word:        "door",
  translation: "obanda",
  type:        "n."
},
{
  word:        "doubt",
  translation: "bhuk",
  type:        "n."
},
{
  word:        "down",
  translation: "moro",
  type:        "adj., adv. & prep."
},
{
  word:        "downcast",
  translation: "mosiev",
  type:        "adj."
},
{
  word:        "dragon",
  translation: "aman (capitalized to mean the Dragon)",
  type:        "n."
},
{
  word:        "drain",
  translation: "vraak",
  type:        "v."
},
{
  word:        "drawer",
  translation: "olghan",
  type:        "n."
},
{
  word:        "dream",
  translation: "hou’dabor",
  type:        "n."
},
{
  word:        "dress",
  translation: "sleesh",
  type:        "n."
},
{
  word:        "drink",
  translation: "probita",
  type:        "v."
},
{
  word:        "drive",
  translation: "manive",
  type:        "v."
},
{
  word:        "driving",
  translation: "manivin",
  type:        "n. & adj."
},
{
  word:        "drop",
  translation: "rumpo",
  type:        "v."
},
{
  word:        "dry",
  translation: "khust",
  type:        "adj."
},
{
  word:        "dust",
  translation: "yekko",
  type:        "n."
},
{
  word:        "duty",
  translation: "toh (used by the Aiel)",
  type:        "n."
},
{
  word:        "eagle(s)",
  translation: "aldazar",
  type:        "n."
},
{
  word:        "ear",
  translation: "habish",
  type:        "n."
},
{
  word:        "early",
  translation: "spondat",
  type:        "adj."
},
{
  word:        "earth",
  translation: "zhoub",
  type:        "n."
},
{
  word:        "east",
  translation: "hochin",
  type:        "n., adj. & adv."
},
{
  word:        "edge",
  translation: "yamar",
  type:        "n."
},
{
  word:        "education",
  translation: "sunatien",
  type:        "n."
},
{
  word:        "effect",
  translation: "ptash",
  type:        "n."
},
{
  word:        "egg",
  translation: "urkros",
  type:        "n."
},
{
  word:        "eight",
  translation: "minyat (material objects); minye (immaterial things)",
  type:        "adj."
},
{
  word:        "elastic",
  translation: "zahert",
  type:        "adj."
},
{
  word:        "emotion",
  translation: "kanjo",
  type:        "n."
},
{
  word:        "Empress",
  translation: "Sh’boan (Sharan term)",
  type:        "n."
},
{
  word:        "Empress’ consort",
  translation: "Sh’botay (Sharan term)",
  type:        "n."
},
{
  word:        "end",
  translation: "velu",
  type:        "v."
},
{
  word:        "enduring",
  translation: "cierto",
  type:        "adj."
},
{
  word:        "enemy",
  translation: "indemela",
  type:        "n."
},
{
  word:        "engine",
  translation: "chicaba",
  type:        "n."
},
{
  word:        "enough",
  translation: "purtah",
  type:        "n., adj. & adv."
},
{
  word:        "entity",
  translation: "so",
  type:        "n."
},
{
  word:        "Envious, the",
  translation: "Be’lal (name of a Forsaken; literally, desire to have)",
  type:        "n."
},
{
  word:        "equal",
  translation: "perit",
  type:        "n., adj. & v."
},
{
  word:        "equivalent value",
  translation: "carentin",
  type:        "n."
},
{
  word:        "eradication",
  translation: "bhan",
  type:        "n."
},
{
  word:        "error",
  translation: "zyntam",
  type:        "n."
},
{
  word:        "essence",
  translation: "balt",
  type:        "n."
},
{
  word:        "essence of youth",
  translation: "balthamel (name of a Forsaken)",
  type:        "n."
},
{
  word:        "even",
  translation: "glasti",
  type:        "adj."
},
{
  word:        "event",
  translation: "whandin",
  type:        "n."
},
{
  word:        "ever",
  translation: "aend",
  type:        "adv."
},
{
  word:        "everyone",
  translation: "aes",
  type:        "n."
},
{
  word:        "exaltation",
  translation: "jhin",
  type:        "n."
},
{
  word:        "example",
  translation: "sidhat",
  type:        "n."
},
{
  word:        "exchange",
  translation: "feiro",
  type:        "v."
},
{
  word:        "existence",
  translation: "humat",
  type:        "n."
},
{
  word:        "expansion",
  translation: "pedalen",
  type:        "n."
},
{
  word:        "experience",
  translation: "proyago",
  type:        "n."
},
{
  word:        "expert",
  translation: "yalait",
  type:        "n. & adj."
},
{
  word:        "exquisite",
  translation: "jheda (name of the royal palace of Ghealdan)",
  type:        "adj."
},
{
  word:        "eye(s)",
  translation: "seia (becomes sei when used in combined forms)",
  type:        "n."
},
{
  word:        "Eye Blinders",
  translation: "Samma N’Sei",
  type:        "n."
},
{
  word:        "eyes, (of) lowered or downcast",
  translation: "sei’mosiev (Seanchan term indicating loss of face or honor)",
  type:        "adj."
},
{
  word:        "eyes, (of) straight or level",
  translation: "sei’taer (Seanchan term indicating having or gaining face)",
  type:        "adj."
},
{
  word:        "face",
  translation: "cheta",
  type:        "n."
},
{
  word:        "fact",
  translation: "papp",
  type:        "n."
},
{
  word:        "fade",
  translation: "vyen",
  type:        "v."
},
{
  word:        "falcon",
  translation: "faile",
  type:        "n."
},
{
  word:        "Falcon’s Talon, the",
  translation: "Cha Faile (name taken by Faile’s followers)",
  type:        "n."
},
{
  word:        "fall",
  translation: "lendha",
  type:        "v."
},
{
  word:        "false",
  translation: "suzain",
  type:        "adj."
},
{
  word:        "family",
  translation: "weladhi",
  type:        "n."
},
{
  word:        "far",
  translation: "totah",
  type:        "adj. & adv."
},
{
  word:        "farm",
  translation: "jemena",
  type:        "n."
},
{
  word:        "fat",
  translation: "boko",
  type:        "adj."
},
{
  word:        "father",
  translation: "dada",
  type:        "n."
},
{
  word:        "father(s)/sire(s)",
  translation: "vol (specific to a male who has used brutal means, i.e., a rapist)",
  type:        "n."
},
{
  word:        "father of ranges",
  translation: "dadaranell (from ancient name for Fal Dara)",
  type:        "n."
},
{
  word:        "favor",
  translation: "taishite",
  type:        "v."
},
{
  word:        "fear",
  translation: "daghain",
  type:        "n."
},
{
  word:        "feather",
  translation: "velin",
  type:        "n."
},
{
  word:        "feeble",
  translation: "gubbel",
  type:        "adj."
},
{
  word:        "feeling",
  translation: "kutya",
  type:        "n."
},
{
  word:        "fertile",
  translation: "whado",
  type:        "adj."
},
{
  word:        "fiction",
  translation: "istor",
  type:        "n."
},
{
  word:        "field",
  translation: "vlagh",
  type:        "n."
},
{
  word:        "fight",
  translation: "alget",
  type:        "v."
},
{
  word:        "fighter",
  translation: "algai",
  type:        "n."
},
{
  word:        "final",
  translation: "tarmon",
  type:        "adj."
},
{
  word:        "finder",
  translation: "jeade",
  type:        "n."
},
{
  word:        "finger",
  translation: "ungost",
  type:        "n."
},
{
  word:        "fire",
  translation: "nabir",
  type:        "n."
},
{
  word:        "first",
  translation: "koyn",
  type:        "n., adj. & adv."
},
{
  word:        "first lover",
  translation: "carneira (Malkieri)",
  type:        "n."
},
{
  word:        "fish",
  translation: "mastri",
  type:        "n."
},
{
  word:        "five",
  translation: "choryat (material objects); chorye (immaterial things)",
  type:        "n. & adj."
},
{
  word:        "fix",
  translation: "ursta",
  type:        "v."
},
{
  word:        "fixed",
  translation: "urstae",
  type:        "adj."
},
{
  word:        "flag",
  translation: "laero",
  type:        "n."
},
{
  word:        "flame",
  translation: "naito",
  type:        "n."
},
{
  word:        "Flame and the Void, the",
  translation: "ko’di (a meditative state)",
  type:        "n."
},
{
  word:        "flat",
  translation: "dzigal",
  type:        "adj."
},
{
  word:        "flight",
  translation: "vavaya",
  type:        "n."
},
{
  word:        "floor",
  translation: "boesin",
  type:        "n."
},
{
  word:        "flower",
  translation: "bijoun",
  type:        "n."
},
{
  word:        "fly",
  translation: "raf",
  type:        "v."
},
{
  word:        "fold",
  translation: "viliso",
  type:        "v."
},
{
  word:        "folk",
  translation: "atha’an (nationhood implied)",
  type:        "n."
},
{
  word:        "food",
  translation: "werstom",
  type:        "n."
},
{
  word:        "foolbox",
  translation: "nar’baha (traveling boxes used by Sammael)",
  type:        "n."
},
{
  word:        "foolish",
  translation: "narfa",
  type:        "adj."
},
{
  word:        "foot/on foot/afoot",
  translation: "muad",
  type:        "n., adj. & adv."
},
{
  word:        "footmen",
  translation: "muad’drin",
  type:        "n."
},
{
  word:        "for",
  translation: "ni",
  type:        "prep."
},
{
  word:        "for the",
  translation: "al; an",
  type:        "prep."
},
{
  word:        "force",
  translation: "nadula",
  type:        "n."
},
{
  word:        "forerunners",
  translation: "hailene (Seanchan term)",
  type:        "n."
},
{
  word:        "forgiveness",
  translation: "hessa",
  type:        "n."
},
{
  word:        "fork",
  translation: "kashen",
  type:        "n."
},
{
  word:        "form",
  translation: "bideli",
  type:        "n."
},
{
  word:        "forthright",
  translation: "taer",
  type:        "adj."
},
{
  word:        "forward (direction)",
  translation: "dar; los (n., adj. & adv.)",
  type:        "adv."
},
{
  word:        "four",
  translation: "otyat (material objects); otye (immaterial things)",
  type:        "adj."
},
{
  word:        "fowl",
  translation: "ayashiel",
  type:        "n."
},
{
  word:        "frame",
  translation: "lamena",
  type:        "n."
},
{
  word:        "free",
  translation: "ayende",
  type:        "v."
},
{
  word:        "free man",
  translation: "caballein",
  type:        "n."
},
{
  word:        "free(dom)",
  translation: "raha",
  type:        "n. & adj."
},
{
  word:        "frequent",
  translation: "moodi",
  type:        "adj."
},
{
  word:        "friend",
  translation: "amela",
  type:        "n."
},
{
  word:        "from",
  translation: "der",
  type:        "prep."
},
{
  word:        "from, derived",
  translation: "dera",
  type:        "suffix"
},
{
  word:        "front",
  translation: "beulin",
  type:        "n. & adj."
},
{
  word:        "full",
  translation: "nahobo",
  type:        "adj."
},
{
  word:        "future",
  translation: "mamai",
  type:        "n. & adj."
},
{
  word:        "game",
  translation: "mar",
  type:        "n."
},
{
  word:        "Game of Houses",
  translation: "Daes Dae’mar",
  type:        "n."
},
{
  word:        "garden",
  translation: "ferster",
  type:        "n."
},
{
  word:        "general",
  translation: "kelindun (military rank)",
  type:        "n."
},
{
  word:        "get",
  translation: "tana",
  type:        "v."
},
{
  word:        "girl",
  translation: "inda",
  type:        "n."
},
{
  word:        "give",
  translation: "nolve",
  type:        "v."
},
{
  word:        "give, you",
  translation: "ma",
  type:        "v."
},
{
  word:        "given, is",
  translation: "nolvae",
  type:        "v."
},
{
  word:        "glass",
  translation: "vartan",
  type:        "n."
},
{
  word:        "glory",
  translation: "daishar; kiserai",
  type:        "n."
},
{
  word:        "Glory of All",
  translation: "Aesdaishar (name of palace in Chachin)",
  type:        "n."
},
{
  word:        "glove",
  translation: "swabel",
  type:        "n."
},
{
  word:        "go",
  translation: "jalou",
  type:        "v."
},
{
  word:        "goat",
  translation: "ozela",
  type:        "n."
},
{
  word:        "gold(en)",
  translation: "cair",
  type:        "n. & adj."
},
{
  word:        "gold(en), the",
  translation: "al’cair",
  type:        "adj."
},
{
  word:        "Golden Bowl, the",
  translation: "Al’Cair Dal (canyon in the Aiel Waste)",
  type:        "n."
},
{
  word:        "Gold(en) Dawn Hill",
  translation: "Al’Cair’Rhaienallen (ancient name for Cairhien)",
  type:        "n."
},
{
  word:        "golden eyes",
  translation: "sei’cair (Aiel title for Perrin)",
  type:        "n."
},
{
  word:        "good",
  translation: "uiwa",
  type:        "adj."
},
{
  word:        "government",
  translation: "vlafael",
  type:        "n."
},
{
  word:        "grain",
  translation: "kazka",
  type:        "n."
},
{
  word:        "grass",
  translation: "zurye",
  type:        "n."
},
{
  word:        "grave",
  translation: "moridin (name of a Forsaken, the word referring to death)",
  type:        "n."
},
{
  word:        "gray",
  translation: "tezra",
  type:        "adj."
},
{
  word:        "great",
  translation: "dae",
  type:        "adj."
},
{
  word:        "Great Game, The",
  translation: "Daes Dae’mar",
  type:        "n."
},
{
  word:        "green",
  translation: "drosin",
  type:        "n. & adj."
},
{
  word:        "grip",
  translation: "duente",
  type:        "v."
},
{
  word:        "group/band/brotherhood",
  translation: "shen",
  type:        "n."
},
{
  word:        "growth",
  translation: "suchan",
  type:        "n."
},
{
  word:        "grub",
  translation: "motai (a specific sweet, crunchy grub found in the Aiel Waste)",
  type:        "n."
},
{
  word:        "guard",
  translation: "ashan",
  type:        "prefix"
},
{
  word:        "guard",
  translation: "valdar",
  type:        "n."
},
{
  word:        "guard",
  translation: "valon",
  type:        "v."
},
{
  word:        "guard of the blade",
  translation: "asha’man",
  type:        "n."
},
{
  word:        "guard sword",
  translation: "ashandarei (Birgitte’s name for Mat’s sword)",
  type:        "n."
},
{
  word:        "guide",
  translation: "ranzak",
  type:        "n. & v."
},
{
  word:        "hag",
  translation: "drova",
  type:        "n."
},
{
  word:        "hag",
  translation: "drovja",
  type:        "possessive"
},
{
  word:        "hair",
  translation: "vokosh",
  type:        "n."
},
{
  word:        "hair cord",
  translation: "daori (cut by Malkieri’s carneira and woven into a cord)",
  type:        "n."
},
{
  word:        "hammer",
  translation: "marcador",
  type:        "n."
},
{
  word:        "hand",
  translation: "har(an)",
  type:        "n."
},
{
  word:        "Hand of the Dark",
  translation: "Shaidar Haran (name of an “extreme” Myrddraal)",
  type:        "n."
},
{
  word:        "hands",
  translation: "sovin (if unmodified, “hands that are open and empty”)",
  type:        "n."
},
{
  word:        "hanging",
  translation: "wafal",
  type:        "n. & adj."
},
{
  word:        "happy",
  translation: "zanzi",
  type:        "adj."
},
{
  word:        "harbor",
  translation: "calazar",
  type:        "n."
},
{
  word:        "hard",
  translation: "batthien",
  type:        "adj."
},
{
  word:        "harmony",
  translation: "aris",
  type:        "n."
},
{
  word:        "Harvesters of Souls",
  translation: "Ghob’hlin (a Trolloc band)",
  type:        "n."
},
{
  word:        "hat",
  translation: "tarbun",
  type:        "n."
},
{
  word:        "hate",
  translation: "nirdayn",
  type:        "n."
},
{
  word:        "have",
  translation: "lal",
  type:        "v."
},
{
  word:        "he",
  translation: "sin",
  type:        "pron."
},
{
  word:        "he who soars",
  translation: "mah’alleinir (name of Perrin’s Power-wrought sword)",
  type:        "n."
},
{
  word:        "head",
  translation: "tiest",
  type:        "n."
},
{
  word:        "headband, Malkieri",
  translation: "hadori",
  type:        "n."
},
{
  word:        "health",
  translation: "shuk",
  type:        "n."
},
{
  word:        "healthy",
  translation: "shukri",
  type:        "adj."
},
{
  word:        "hearing",
  translation: "bhashan",
  type:        "n."
},
{
  word:        "heart",
  translation: "balt; corda (as in, the heart of things); cuendar (can also be adj.)",
  type:        "n."
},
{
  word:        "Heart Guard, the",
  translation: "Valdar Cuebeyari (Heart refers to the Nation/People/Land)",
  type:        "n."
},
{
  word:        "heart, my",
  translation: "cuebiyar (when capitalized, it refers to heart of a nation/people/ruler)",
  type:        "n."
},
{
  word:        "Heart of the Dark",
  translation: "Ba’alzamon (another name for the Forsaken Ishamael)",
  type:        "n."
},
{
  word:        "Heart of the People",
  translation: "Cordamora (the palace in Maradon)",
  type:        "n."
},
{
  word:        "heartstone",
  translation: "cuendillar",
  type:        "n."
},
{
  word:        "heat",
  translation: "jalid",
  type:        "n."
},
{
  word:        "help",
  translation: "spiat",
  type:        "v."
},
{
  word:        "herd",
  translation: "for",
  type:        "n."
},
{
  word:        "here",
  translation: "kodome",
  type:        "n. & adv."
},
{
  word:        "high",
  translation: "mund",
  type:        "adj."
},
{
  word:        "hill",
  translation: "allen",
  type:        "n."
},
{
  word:        "history",
  translation: "khalig",
  type:        "n."
},
{
  word:        "hold",
  translation: "sul; duente",
  type:        "v."
},
{
  word:        "hole",
  translation: "ghul",
  type:        "n."
},
{
  word:        "hollow",
  translation: "ghow",
  type:        "adj."
},
{
  word:        "Homecomers",
  translation: "Rhyagelle (Seanchan term)",
  type:        "n."
},
{
  word:        "honor",
  translation: "carai (can be used in the sense of “for the honor”); ji; kiserai",
  type:        "n."
},
{
  word:        "honor",
  translation: "tsingu",
  type:        "n. & v."
},
{
  word:        "honor and obligation",
  translation: "ji’e’toh (used by Aiel)",
  type:        "n."
},
{
  word:        "honorable one",
  translation: "kiseran (Siuan addressed Loial in this manner)",
  type:        "n."
},
{
  word:        "hook",
  translation: "zhoh",
  type:        "n."
},
{
  word:        "hope",
  translation: "mael",
  type:        "n."
},
{
  word:        "horn",
  translation: "purvene",
  type:        "n."
},
{
  word:        "horror",
  translation: "dhjin",
  type:        "n."
},
{
  word:        "horse",
  translation: "caba",
  type:        "n."
},
{
  word:        "horse to ride",
  translation: "caba’donde",
  type:        "n."
},
{
  word:        "horseman",
  translation: "caba’drin; caballein",
  type:        "n."
},
{
  word:        "hospital",
  translation: "ospouin",
  type:        "n."
},
{
  word:        "hour",
  translation: "baroc",
  type:        "n."
},
{
  word:        "house",
  translation: "shaek",
  type:        "n."
},
{
  word:        "how",
  translation: "bak",
  type:        "adv."
},
{
  word:        "humor",
  translation: "salidien",
  type:        "n."
},
{
  word:        "hundred",
  translation: "deshi (meaning one hundred)",
  type:        "n. & adj."
},
{
  word:        "hundreds",
  translation: "deshi (denotes the quantitative place value)",
  type:        "suffix"
},
{
  word:        "I",
  translation: "ye (sometimes used as an exclamatory fragment)",
  type:        "pron."
},
{
  word:        "ice",
  translation: "youst",
  type:        "n."
},
{
  word:        "idea",
  translation: "prasta",
  type:        "n."
},
{
  word:        "if",
  translation: "sob",
  type:        "conj."
},
{
  word:        "ill",
  translation: "bokhen",
  type:        "adj."
},
{
  word:        "important",
  translation: "matuet",
  type:        "adj."
},
{
  word:        "impulse",
  translation: "obram",
  type:        "n."
},
{
  word:        "in",
  translation: "sa",
  type:        "adv. & prep."
},
{
  word:        "increase",
  translation: "whakatu",
  type:        "v."
},
{
  word:        "individual",
  translation: "da (either gender)",
  type:        "n."
},
{
  word:        "industry",
  translation: "santhal",
  type:        "n."
},
{
  word:        "infantry(men)",
  translation: "muad’drin",
  type:        "n."
},
{
  word:        "ink",
  translation: "veel",
  type:        "n."
},
{
  word:        "inn",
  translation: "melaz",
  type:        "n."
},
{
  word:        "insect",
  translation: "tanilji",
  type:        "n."
},
{
  word:        "instrument",
  translation: "tatatoun",
  type:        "n."
},
{
  word:        "insurance",
  translation: "straviant",
  type:        "n."
},
{
  word:        "interest",
  translation: "dibbuk",
  type:        "n."
},
{
  word:        "intricate",
  translation: "dae",
  type:        "adj."
},
{
  word:        "intricate game",
  translation: "dae’mar",
  type:        "n."
},
{
  word:        "invention",
  translation: "havokiz",
  type:        "n."
},
{
  word:        "iron",
  translation: "shiatar",
  type:        "n. & adj."
},
{
  word:        "is",
  translation: "ain; ga; isain (a form of ‘to be’); sain",
  type:        "v."
},
{
  word:        "is no(t)",
  translation: "isainde (insistent; emphatic)",
  type:        "v. neg."
},
{
  word:        "island",
  translation: "chanukar",
  type:        "n."
},
{
  word:        "it",
  translation: "aso",
  type:        "pron."
},
{
  word:        "it(self)",
  translation: "se",
  type:        "pron."
},
{
  word:        "jewel",
  translation: "keisa",
  type:        "n."
},
{
  word:        "jewelry",
  translation: "kesiera (worn on forehead, as did Moiraine); ki’sain (Malkieri woman’s forehead adornment)",
  type:        "n."
},
{
  word:        "join",
  translation: "heatsu",
  type:        "v."
},
{
  word:        "journey",
  translation: "shin",
  type:        "n."
},
{
  word:        "Journey of Destruction",
  translation: "Machin Shin (aka Black Wind)",
  type:        "n."
},
{
  word:        "judge",
  translation: "vodish",
  type:        "n. & v."
},
{
  word:        "jump",
  translation: "shao",
  type:        "v."
},
{
  word:        "keep",
  translation: "tyaku",
  type:        "v."
},
{
  word:        "kennel",
  translation: "rainn",
  type:        "n."
},
{
  word:        "kettle",
  translation: "jalbouk",
  type:        "n."
},
{
  word:        "key",
  translation: "allwair",
  type:        "n."
},
{
  word:        "kick",
  translation: "kathana",
  type:        "v."
},
{
  word:        "kind",
  translation: "yappa",
  type:        "adj."
},
{
  word:        "kiss",
  translation: "brith",
  type:        "n. & v."
},
{
  word:        "kitchen",
  translation: "rensal",
  type:        "n."
},
{
  word:        "knee",
  translation: "shost",
  type:        "n."
},
{
  word:        "knife",
  translation: "nai",
  type:        "n."
},
{
  word:        "Knife Hands",
  translation: "Sovin Nai (Aiel warrior society)",
  type:        "n."
},
{
  word:        "knot",
  translation: "buido",
  type:        "n. & v."
},
{
  word:        "knowledge",
  translation: "sanasant",
  type:        "n."
},
{
  word:        "lacking",
  translation: "mera",
  type:        "prep."
},
{
  word:        "land",
  translation: "dhol",
  type:        "n."
},
{
  word:        "land, a",
  translation: "rhiod",
  type:        "n."
},
{
  word:        "land of harmony",
  translation: "aridhol (a city of the Second Convenant)",
  type:        "n."
},
{
  word:        "language",
  translation: "domorakoshi",
  type:        "n."
},
{
  word:        "last",
  translation: "cyn; tarmon (adj.)",
  type:        "n. & adj."
},
{
  word:        "Last Battle, the",
  translation: "Tarmon Gai’don (not italicized because of everyday usage)",
  type:        "n."
},
{
  word:        "last chance",
  translation: "cyndane (name given to reincarnated Lanfear)",
  type:        "n."
},
{
  word:        "late",
  translation: "unyat",
  type:        "adj. & adv."
},
{
  word:        "laugh",
  translation: "kuthli",
  type:        "v."
},
{
  word:        "law",
  translation: "vidhel",
  type:        "n."
},
{
  word:        "lead",
  translation: "hael",
  type:        "v."
},
{
  word:        "leader",
  translation: "m’hael (if capitalized, implies ‘Supreme Leader’, per Taim)",
  type:        "n."
},
{
  word:        "Leader of the Attack",
  translation: "Baijan’m’hael",
  type:        "n."
},
{
  word:        "learning",
  translation: "koanto",
  type:        "n."
},
{
  word:        "leash",
  translation: "a’dam (used by Seanchan); dam",
  type:        "n."
},
{
  word:        "leash holder",
  translation: "sul’dam (Seanchan term)",
  type:        "n."
},
{
  word:        "leashed",
  translation: "damane (used by Seanchan to mean leashed one)",
  type:        "n."
},
{
  word:        "leather",
  translation: "parikesh",
  type:        "n."
},
{
  word:        "left-hand or left-side",
  translation: "osan",
  type:        "adj."
},
{
  word:        "left-hand dagger",
  translation: "osan’gar (name of a Forsaken)",
  type:        "n."
},
{
  word:        "leg",
  translation: "uvaal",
  type:        "n."
},
{
  word:        "lessons",
  translation: "mestani",
  type:        "n."
},
{
  word:        "lessons, teacher of",
  translation: "mesaana (name of a Forsaken)",
  type:        "n."
},
{
  word:        "let",
  translation: "youna",
  type:        "v."
},
{
  word:        "lethal weapon",
  translation: "gar",
  type:        "n."
},
{
  word:        "letter",
  translation: "brett",
  type:        "n."
},
{
  word:        "level",
  translation: "taer",
  type:        "adj."
},
{
  word:        "library",
  translation: "sa’blagh",
  type:        "n."
},
{
  word:        "lies",
  translation: "ansoen",
  type:        "n."
},
{
  word:        "life",
  translation: "sora",
  type:        "n."
},
{
  word:        "lift",
  translation: "padgi",
  type:        "v."
},
{
  word:        "light",
  translation: "dival",
  type:        "n."
},
{
  word:        "like",
  translation: "sene",
  type:        "v. & adv."
},
{
  word:        "limit",
  translation: "ghleb",
  type:        "n."
},
{
  word:        "line",
  translation: "pinikar",
  type:        "n."
},
{
  word:        "linen",
  translation: "shaval",
  type:        "n. & adj."
},
{
  word:        "lip",
  translation: "kadu",
  type:        "n."
},
{
  word:        "liquid",
  translation: "budhvai",
  type:        "adj."
},
{
  word:        "list",
  translation: "kikola",
  type:        "n."
},
{
  word:        "little",
  translation: "min",
  type:        "adj."
},
{
  word:        "living",
  translation: "airach",
  type:        "n. & adj."
},
{
  word:        "living organism",
  translation: "agit",
  type:        "n."
},
{
  word:        "lizard",
  translation: "gara (a specific, poisonous creature from Aiel Waste)",
  type:        "n."
},
{
  word:        "lock",
  translation: "clomak",
  type:        "n."
},
{
  word:        "long",
  translation: "laada",
  type:        "adj"
},
{
  word:        "look",
  translation: "gouql",
  type:        "v."
},
{
  word:        "loose",
  translation: "woudem",
  type:        "adj."
},
{
  word:        "lord",
  translation: "shan",
  type:        "n."
},
{
  word:        "Lord of Glory",
  translation: "Tai’daishar (literally, True Glory or True Blood of Battle; name of Rand’s horse)",
  type:        "n."
},
{
  word:        "loss",
  translation: "thorain",
  type:        "n."
},
{
  word:        "lost",
  translation: "niende",
  type:        "adj."
},
{
  word:        "loud",
  translation: "mad",
  type:        "adj."
},
{
  word:        "loud noise",
  translation: "sha’mad (thunder)",
  type:        "n."
},
{
  word:        "love",
  translation: "mashi",
  type:        "n. & v."
},
{
  word:        "love, my",
  translation: "mashiara (a hopeless love, maybe already lost)",
  type:        "n."
},
{
  word:        "lover, first",
  translation: "carneira (Malkieri)",
  type:        "n."
},
{
  word:        "low",
  translation: "mosai",
  type:        "adj."
},
{
  word:        "lower",
  translation: "szere",
  type:        "adj."
},
{
  word:        "lower",
  translation: "mosiel",
  type:        "v."
},
{
  word:        "lowered",
  translation: "mosiev",
  type:        "adj."
},
{
  word:        "luck",
  translation: "dovienya",
  type:        "n."
},
{
  word:        "lucky",
  translation: "dovie (or, related to luck)",
  type:        "adj."
},
{
  word:        "machine",
  translation: "worshi",
  type:        "n."
},
{
  word:        "made, is",
  translation: "gemarisae",
  type:        "v."
},
{
  word:        "maiden(s)",
  translation: "mai",
  type:        "n."
},
{
  word:        "Maidens of the Spear",
  translation: "Far Dareis Mai (an Aiel warrior society)",
  type:        "n."
},
{
  word:        "maize",
  translation: "zemai (from Aiel Waste)",
  type:        "n."
},
{
  word:        "majesty",
  translation: "deyeniye",
  type:        "n."
},
{
  word:        "make",
  translation: "gemarise; tasu",
  type:        "v."
},
{
  word:        "man",
  translation: "allein; drin (sing. & plur.); sin",
  type:        "n."
},
{
  word:        "manage",
  translation: "rabat",
  type:        "v."
},
{
  word:        "manure",
  translation: "choss (excrement hauled away on farms)",
  type:        "n."
},
{
  word:        "many",
  translation: "es",
  type:        "adj. suffix"
},
{
  word:        "many people",
  translation: "daes",
  type:        "n."
},
{
  word:        "map",
  translation: "procol",
  type:        "n."
},
{
  word:        "mark",
  translation: "rouyte",
  type:        "n."
},
{
  word:        "market",
  translation: "zladtar",
  type:        "n."
},
{
  word:        "married",
  translation: "cassort",
  type:        "adj."
},
{
  word:        "mass",
  translation: "pentor",
  type:        "v."
},
{
  word:        "master",
  translation: "der (Seanchan prefix to denote master of some craft)",
  type:        "n. prefix"
},
{
  word:        "match",
  translation: "wanda",
  type:        "v."
},
{
  word:        "material",
  translation: "loftan",
  type:        "n."
},
{
  word:        "may",
  translation: "punia",
  type:        "v."
},
{
  word:        "me",
  translation: "mia; no",
  type:        "pron."
},
{
  word:        "Me, One Who Owns",
  translation: "Mia’cova (term used by Moghedien, after the mindtrap)",
  type:        "n."
},
{
  word:        "meal",
  translation: "witapa",
  type:        "n."
},
{
  word:        "measure",
  translation: "gheym",
  type:        "n. & v."
},
{
  word:        "meat",
  translation: "muaghde",
  type:        "n."
},
{
  word:        "medical",
  translation: "restar",
  type:        "adj."
},
{
  word:        "meeting",
  translation: "lashite",
  type:        "n."
},
{
  word:        "memory/memories",
  translation: "loviyaga/loviyagae",
  type:        "n."
},
{
  word:        "metal",
  translation: "caledon",
  type:        "n."
},
{
  word:        "middle",
  translation: "mist",
  type:        "n. & adj."
},
{
  word:        "mighty, the",
  translation: "ghraem",
  type:        "n."
},
{
  word:        "military",
  translation: "lennito",
  type:        "n. & adj."
},
{
  word:        "milk",
  translation: "mokol",
  type:        "n."
},
{
  word:        "mind",
  translation: "souvra",
  type:        "n."
},
{
  word:        "mind, my own",
  translation: "souvraya",
  type:        "comb."
},
{
  word:        "mindtrap",
  translation: "cour’souvra (used on the Forsaken)",
  type:        "n."
},
{
  word:        "mine",
  translation: "raya",
  type:        "poss. pron."
},
{
  word:        "minute",
  translation: "glimp",
  type:        "n."
},
{
  word:        "mist",
  translation: "neb",
  type:        "n."
},
{
  word:        "mixed",
  translation: "komad",
  type:        "adj."
},
{
  word:        "mobility, indication of",
  translation: "far",
  type:        "prep."
},
{
  word:        "money",
  translation: "dhalen",
  type:        "n."
},
{
  word:        "monkey",
  translation: "lindhi",
  type:        "n."
},
{
  word:        "month",
  translation: "nakhino",
  type:        "n."
},
{
  word:        "moon",
  translation: "bumma",
  type:        "n."
},
{
  word:        "morning",
  translation: "morasu",
  type:        "n."
},
{
  word:        "mother",
  translation: "mamu",
  type:        "n."
},
{
  word:        "motion",
  translation: "lakevan",
  type:        "n."
},
{
  word:        "mountain(s)",
  translation: "dore",
  type:        "n. & adj."
},
{
  word:        "mountains, of/from the",
  translation: "n’dore",
  type:        "adj."
},
{
  word:        "Mountain Dancers",
  translation: "Hama N’dore (Aiel warrior society)",
  type:        "n."
},
{
  word:        "Mountain Home",
  translation: "Manetheren (one of the Ten Nations)",
  type:        "n."
},
{
  word:        "mountain range(s)",
  translation: "ranell",
  type:        "n."
},
{
  word:        "mouth",
  translation: "mafal",
  type:        "n."
},
{
  word:        "move",
  translation: "vakar",
  type:        "v."
},
{
  word:        "much",
  translation: "plean",
  type:        "adj. & adv."
},
{
  word:        "muscle",
  translation: "hathi",
  type:        "n."
},
{
  word:        "mushroom(s)",
  translation: "mourets",
  type:        "n."
},
{
  word:        "music",
  translation: "asmodi",
  type:        "n."
},
{
  word:        "musician",
  translation: "asmodean (name of a Forsaken)",
  type:        "n."
},
{
  word:        "my",
  translation: "mi",
  type:        "poss. pron."
},
{
  word:        "my own",
  translation: "raya",
  type:        "poss. pron."
},
{
  word:        "My Owner",
  translation: "Mia’cova (Moghedien, after the mindtrap)",
  type:        "n."
},
{
  word:        "myself",
  translation: "mia",
  type:        "pron."
},
{
  word:        "nail",
  translation: "hakhel",
  type:        "n."
},
{
  word:        "name",
  translation: "yalu",
  type:        "n. & v."
},
{
  word:        "narrow",
  translation: "zengar",
  type:        "adj."
},
{
  word:        "natural",
  translation: "dyani",
  type:        "adj."
},
{
  word:        "near",
  translation: "ragha",
  type:        "adj., adv. & prep."
},
{
  word:        "necessary",
  translation: "maast",
  type:        "adj."
},
{
  word:        "necessity",
  translation: "mestrak",
  type:        "n."
},
{
  word:        "neck",
  translation: "liede",
  type:        "n."
},
{
  word:        "need",
  translation: "zeltain",
  type:        "n."
},
{
  word:        "needle, type of",
  translation: "conje (mentioned by Sammael)",
  type:        "n."
},
{
  word:        "nephew",
  translation: "finin",
  type:        "n."
},
{
  word:        "nerve",
  translation: "wagg",
  type:        "n."
},
{
  word:        "net",
  translation: "upendar",
  type:        "n."
},
{
  word:        "never",
  translation: "sind",
  type:        "adv."
},
{
  word:        "new",
  translation: "neidu",
  type:        "adj."
},
{
  word:        "news",
  translation: "canant",
  type:        "n."
},
{
  word:        "niece",
  translation: "qinar",
  type:        "n."
},
{
  word:        "night",
  translation: "anfear; cor; fear",
  type:        "n."
},
{
  word:        "Night Spears",
  translation: "Cor Darei (Aiel warrior society)",
  type:        "n."
},
{
  word:        "nine",
  translation: "navyat (material objects); navye (immaterial things)",
  type:        "n. & adj."
},
{
  word:        "no",
  translation: "inde (a general negation)",
  type:        "n. & adv."
},
{
  word:        "noise",
  translation: "sha",
  type:        "n."
},
{
  word:        "none",
  translation: "pas",
  type:        "pron."
},
{
  word:        "normal",
  translation: "gidhi",
  type:        "adj."
},
{
  word:        "north",
  translation: "torreale",
  type:        "n., adj. & adv."
},
{
  word:        "nose",
  translation: "nothru",
  type:        "n."
},
{
  word:        "not",
  translation: "inde (a general negation)",
  type:        "n. & adv."
},
{
  word:        "note",
  translation: "dahid",
  type:        "n."
},
{
  word:        "now",
  translation: "waji",
  type:        "n. & adv."
},
{
  word:        "number",
  translation: "punta",
  type:        "n."
},
{
  word:        "nut",
  translation: "doti",
  type:        "n."
},
{
  word:        "oath",
  translation: "gurupat",
  type:        "n."
},
{
  word:        "obligation",
  translation: "toh (used by the Aiel)",
  type:        "n."
},
{
  word:        "obscenity, an",
  translation: "kjasic (spoken by Sammael)",
  type:        "adj."
},
{
  word:        "observation",
  translation: "lahdin",
  type:        "n."
},
{
  word:        "obstinate",
  translation: "seren",
  type:        "adj."
},
{
  word:        "ocean",
  translation: "miere",
  type:        "n."
},
{
  word:        "of",
  translation: "a; an; (implied ownership or inferior position); far",
  type:        "prep."
},
{
  word:        "of the",
  translation: "al; an",
  type:        "prep."
},
{
  word:        "of the fallen/dead",
  translation: "ayend’an",
  type:        "prep."
},
{
  word:        "of the power to channel",
  translation: "angreal (device that enhances the power to channel)",
  type:        "n."
},
{
  word:        "of the power to channel (limited or specific application)",
  translation: "ter’angreal",
  type:        "n."
},
{
  word:        "of the power to channel (superlative)",
  translation: "sa’angreal",
  type:        "n."
},
{
  word:        "off",
  translation: "wek",
  type:        "prep."
},
{
  word:        "offer",
  translation: "homa",
  type:        "v."
},
{
  word:        "office",
  translation: "mystvo",
  type:        "n."
},
{
  word:        "oil",
  translation: "hoba",
  type:        "n."
},
{
  word:        "old",
  translation: "caisen",
  type:        "adj."
},
{
  word:        "on",
  translation: "ost",
  type:        "prep."
},
{
  word:        "one",
  translation: "aan (masc.); da (either gender)",
  type:        "n. & adj."
},
{
  word:        "one",
  translation: "koyat (material objects); koye (immaterial things)",
  type:        "numerical adj."
},
{
  word:        "One Man",
  translation: "Aan’allein (Aiel term for Lan)",
  type:        "n."
},
{
  word:        "one who must be leashed",
  translation: "marath’damane (Seanchan word)",
  type:        "n."
},
{
  word:        "One Who Owns Me",
  translation: "Mia’cova (used by Moghedien, after the mindtrap)",
  type:        "n."
},
{
  word:        "one who twists a blade",
  translation: "demandred (name of a Forsaken)",
  type:        "n."
},
{
  word:        "Oneness, the",
  translation: "ko’di (a meditative state)",
  type:        "n."
},
{
  word:        "only",
  translation: "noup",
  type:        "adj. & adv."
},
{
  word:        "only true",
  translation: "jenn (implying the others are fake)",
  type:        "adj. & adv."
},
{
  word:        "open",
  translation: "ortu",
  type:        "adj."
},
{
  word:        "operation",
  translation: "paathala",
  type:        "n."
},
{
  word:        "opinion",
  translation: "haar",
  type:        "n."
},
{
  word:        "opposite",
  translation: "vyropat",
  type:        "n., adj. & prep."
},
{
  word:        "or",
  translation: "ob",
  type:        "conj."
},
{
  word:        "order",
  translation: "kasaar",
  type:        "n."
},
{
  word:        "organization",
  translation: "vyashak",
  type:        "n."
},
{
  word:        "ornament",
  translation: "casgard",
  type:        "n."
},
{
  word:        "our",
  translation: "fel",
  type:        "poss. pron."
},
{
  word:        "out",
  translation: "keesh",
  type:        "adv. & prep."
},
{
  word:        "oven",
  translation: "roban",
  type:        "n."
},
{
  word:        "over",
  translation: "do",
  type:        "prep."
},
{
  word:        "owned",
  translation: "covale (Seanchan term for slave)",
  type:        "n. & adj."
},
{
  word:        "owner",
  translation: "cova",
  type:        "n."
},
{
  word:        "page",
  translation: "leagh",
  type:        "n."
},
{
  word:        "paid, is",
  translation: "dalae",
  type:        "past part."
},
{
  word:        "pain",
  translation: "mirhage (or the promise or expectation of pain)",
  type:        "n."
},
{
  word:        "pain, the promise or embodiment of",
  translation: "semirhage (name of a Forsaken)",
  type:        "n."
},
{
  word:        "paint",
  translation: "mahrba",
  type:        "v."
},
{
  word:        "pants",
  translation: "olesti",
  type:        "n."
},
{
  word:        "paper",
  translation: "pepa",
  type:        "n."
},
{
  word:        "parallel",
  translation: "naparet",
  type:        "adj."
},
{
  word:        "parcel",
  translation: "zemliat",
  type:        "n."
},
{
  word:        "part",
  translation: "hafi",
  type:        "n."
},
{
  word:        "pass",
  translation: "allende (as passing through or by, not handing something over)",
  type:        "v."
},
{
  word:        "pass",
  translation: "mafal",
  type:        "n."
},
{
  word:        "Pass At the Father of Mountain Ranges",
  translation: "Mafal Dadaranell (ancient name of Fal Dara)",
  type:        "n."
},
{
  word:        "past",
  translation: "culieb",
  type:        "n. & adj."
},
{
  word:        "paste",
  translation: "hasta",
  type:        "n."
},
{
  word:        "Pattern, those who alter or are tied to",
  translation: "ta’veren (those who cause the fabric of the Pattern to bend around them)",
  type:        "n."
},
{
  word:        "Pattern-related",
  translation: "ta",
  type:        "n."
},
{
  word:        "pay",
  translation: "dale",
  type:        "v."
},
{
  word:        "payment",
  translation: "peast",
  type:        "n."
},
{
  word:        "peace",
  translation: "shain; suravye",
  type:        "n."
},
{
  word:        "peach",
  translation: "pierskoe",
  type:        "n."
},
{
  word:        "pen",
  translation: "perol",
  type:        "n."
},
{
  word:        "pencil",
  translation: "olivem",
  type:        "n."
},
{
  word:        "people",
  translation: "atha’an (nationhood implied)",
  type:        "n."
},
{
  word:        "people, the",
  translation: "mora",
  type:        "n."
},
{
  word:        "People Dedicated to Peace",
  translation: "Da’shain (an Aiel term)",
  type:        "n."
},
{
  word:        "People of the Ocean or Waves",
  translation: "Atha’an Miere (Sea Folk)",
  type:        "n."
},
{
  word:        "People of the Shadow",
  translation: "Atha’an Shadar (Darkfriend)",
  type:        "n."
},
{
  word:        "person",
  translation: "atha; da (either gender)",
  type:        "n."
},
{
  word:        "physical",
  translation: "yaati",
  type:        "adj."
},
{
  word:        "picture",
  translation: "dabor",
  type:        "n."
},
{
  word:        "piece, a small",
  translation: "rhub",
  type:        "n."
},
{
  word:        "pig",
  translation: "orcel",
  type:        "n."
},
{
  word:        "pig, tusked water",
  translation: "nedar (type of pig found in the Drowned Lands)",
  type:        "n."
},
{
  word:        "pin",
  translation: "killo",
  type:        "n."
},
{
  word:        "pipe",
  translation: "zalabadh",
  type:        "n."
},
{
  word:        "pit",
  translation: "ghul",
  type:        "n."
},
{
  word:        "place",
  translation: "fonnai",
  type:        "n."
},
{
  word:        "place of waiting",
  translation: "logoth",
  type:        "n."
},
{
  word:        "plane",
  translation: "undacar",
  type:        "n."
},
{
  word:        "plant",
  translation: "gobhat",
  type:        "n."
},
{
  word:        "plate",
  translation: "tippat",
  type:        "n."
},
{
  word:        "play",
  translation: "buggel",
  type:        "v."
},
{
  word:        "please",
  translation: "graedo",
  type:        "v."
},
{
  word:        "pleasure",
  translation: "graen",
  type:        "n."
},
{
  word:        "plow",
  translation: "orichu",
  type:        "n. & v."
},
{
  word:        "pocket",
  translation: "viboin",
  type:        "n."
},
{
  word:        "point",
  translation: "tunga",
  type:        "n."
},
{
  word:        "poison",
  translation: "zintabar",
  type:        "n."
},
{
  word:        "polish",
  translation: "evierto",
  type:        "v."
},
{
  word:        "political",
  translation: "dekhar",
  type:        "adj."
},
{
  word:        "poor",
  translation: "olma",
  type:        "n. & adj."
},
{
  word:        "population, a",
  translation: "mora",
  type:        "n."
},
{
  word:        "porter",
  translation: "wahati",
  type:        "n."
},
{
  word:        "position",
  translation: "obiyar",
  type:        "n."
},
{
  word:        "possible",
  translation: "mozhlit",
  type:        "adj."
},
{
  word:        "pot",
  translation: "wixi",
  type:        "n."
},
{
  word:        "potato",
  translation: "patomi",
  type:        "n."
},
{
  word:        "powder",
  translation: "zipan",
  type:        "n."
},
{
  word:        "power to channel, the",
  translation: "greal",
  type:        "n."
},
{
  word:        "Power, the (female side)",
  translation: "saidar",
  type:        "n."
},
{
  word:        "Power, the (male side)",
  translation: "saidin",
  type:        "n."
},
{
  word:        "prepare (insistent)",
  translation: "rhadiem",
  type:        "v."
},
{
  word:        "present",
  translation: "platip",
  type:        "n. & adj."
},
{
  word:        "price",
  translation: "ciyat",
  type:        "n."
},
{
  word:        "print",
  translation: "orvieda",
  type:        "v."
},
{
  word:        "prison",
  translation: "nayabo",
  type:        "n."
},
{
  word:        "private",
  translation: "mukhrat",
  type:        "adj."
},
{
  word:        "prized",
  translation: "lan",
  type:        "adj."
},
{
  word:        "Prized of the Mighty",
  translation: "Ghraem’lan (a Trolloc band)",
  type:        "n."
},
{
  word:        "probable",
  translation: "tebout",
  type:        "adj."
},
{
  word:        "process",
  translation: "prashat",
  type:        "n."
},
{
  word:        "produce",
  translation: "nodavat",
  type:        "n."
},
{
  word:        "profit",
  translation: "gashi",
  type:        "v."
},
{
  word:        "promise",
  translation: "o’vin; vin",
  type:        "n."
},
{
  word:        "promise of freedom",
  translation: "rahvin (name of a Forsaken)",
  type:        "n."
},
{
  word:        "property",
  translation: "covale (Seanchan term for slave)",
  type:        "n. & adj."
},
{
  word:        "prose",
  translation: "chatkar",
  type:        "n."
},
{
  word:        "protest",
  translation: "pastien",
  type:        "v."
},
{
  word:        "pull",
  translation: "tirast",
  type:        "v."
},
{
  word:        "pump",
  translation: "harvo",
  type:        "v."
},
{
  word:        "punishment",
  translation: "karagaeth",
  type:        "n."
},
{
  word:        "purpose",
  translation: "ghani",
  type:        "n."
},
{
  word:        "push",
  translation: "druna",
  type:        "v."
},
{
  word:        "put",
  translation: "komo",
  type:        "v."
},
{
  word:        "quality",
  translation: "shaani",
  type:        "n."
},
{
  word:        "quarrymen, a country with",
  translation: "s’Gandin (a place from Age of Legends mentioned by Brigitte)",
  type:        "n."
},
{
  word:        "question",
  translation: "feist",
  type:        "v."
},
{
  word:        "quick",
  translation: "kakamo",
  type:        "adj."
},
{
  word:        "quiet",
  translation: "bebak",
  type:        "adj."
},
{
  word:        "quite",
  translation: "koult",
  type:        "adv."
},
{
  word:        "radiance",
  translation: "sidama",
  type:        "n."
},
{
  word:        "rail",
  translation: "griest",
  type:        "n."
},
{
  word:        "rain",
  translation: "ombrede",
  type:        "n. & v."
},
{
  word:        "rat",
  translation: "soetam (large species found in the Drowned Lands); sorda (species found in the Aiel Waste)",
  type:        "n."
},
{
  word:        "rate",
  translation: "chegham",
  type:        "n."
},
{
  word:        "ray",
  translation: "varma",
  type:        "n."
},
{
  word:        "reaction",
  translation: "mawaith",
  type:        "n."
},
{
  word:        "reading",
  translation: "jaahni",
  type:        "n."
},
{
  word:        "ready",
  translation: "tashi",
  type:        "adj."
},
{
  word:        "reason",
  translation: "taskel",
  type:        "n."
},
{
  word:        "receipt",
  translation: "velach",
  type:        "n."
},
{
  word:        "record",
  translation: "tefara",
  type:        "n."
},
{
  word:        "red",
  translation: "cal; dor",
  type:        "n. & adj."
},
{
  word:        "red eagle",
  translation: "al caldazar; caldazar",
  type:        "n."
},
{
  word:        "red hand",
  translation: "calhar",
  type:        "n."
},
{
  word:        "Red Shields",
  translation: "Aethan Dor (an Aiel warrior society)",
  type:        "n."
},
{
  word:        "regret",
  translation: "whudra",
  type:        "n. & v."
},
{
  word:        "regular",
  translation: "pravilam",
  type:        "adj."
},
{
  word:        "relation",
  translation: "rodinat",
  type:        "n."
},
{
  word:        "release",
  translation: "ayende",
  type:        "v."
},
{
  word:        "representative",
  translation: "xurzan",
  type:        "n."
},
{
  word:        "request",
  translation: "saizo",
  type:        "v."
},
{
  word:        "resolute",
  translation: "cierto",
  type:        "adj."
},
{
  word:        "respect",
  translation: "tyagani",
  type:        "n."
},
{
  word:        "responsible",
  translation: "hodifo",
  type:        "adj."
},
{
  word:        "rest",
  translation: "yasipa",
  type:        "v."
},
{
  word:        "Return, The",
  translation: "Corenne (Seanchan concept)",
  type:        "n."
},
{
  word:        "reward",
  translation: "labani",
  type:        "n."
},
{
  word:        "rhythm",
  translation: "bodong",
  type:        "n."
},
{
  word:        "rice",
  translation: "rhaul",
  type:        "n."
},
{
  word:        "ride",
  translation: "donde (or, riding-related)",
  type:        "v."
},
{
  word:        "right-hand or right-side",
  translation: "aran",
  type:        "adj."
},
{
  word:        "right-hand dagger",
  translation: "aran’gar (name of a reborn Forsaken)",
  type:        "n."
},
{
  word:        "ring",
  translation: "onguli",
  type:        "n."
},
{
  word:        "road",
  translation: "rastra",
  type:        "n."
},
{
  word:        "rod",
  translation: "kelet",
  type:        "n."
},
{
  word:        "roll",
  translation: "tovya",
  type:        "v."
},
{
  word:        "roof",
  translation: "chelan",
  type:        "n."
},
{
  word:        "room",
  translation: "zemya",
  type:        "n."
},
{
  word:        "root",
  translation: "balt",
  type:        "n."
},
{
  word:        "rose",
  translation: "ande",
  type:        "n."
},
{
  word:        "Rose of the Sun, the",
  translation: "Ellisande (what the last queen of Manetheren, Eldrene, was called)",
  type:        "n."
},
{
  word:        "rough",
  translation: "xazzi",
  type:        "adj."
},
{
  word:        "round",
  translation: "rulli",
  type:        "adj."
},
{
  word:        "rub",
  translation: "terta",
  type:        "v."
},
{
  word:        "ruin",
  translation: "knotai",
  type:        "n."
},
{
  word:        "rule",
  translation: "vastri",
  type:        "n. & v."
},
{
  word:        "run",
  translation: "sorbe",
  type:        "v."
},
{
  word:        "runner(s)",
  translation: "sorei",
  type:        "n."
},
{
  word:        "sad",
  translation: "fada",
  type:        "adj."
},
{
  word:        "safe",
  translation: "tumasen",
  type:        "adj."
},
{
  word:        "sail",
  translation: "fakha",
  type:        "v."
},
{
  word:        "salt",
  translation: "zela",
  type:        "n."
},
{
  word:        "same",
  translation: "qaiset",
  type:        "pron., adj. & adv."
},
{
  word:        "sand",
  translation: "xentro",
  type:        "n."
},
{
  word:        "say",
  translation: "kazath",
  type:        "v."
},
{
  word:        "scale",
  translation: "cloriol",
  type:        "n."
},
{
  word:        "school",
  translation: "arkati",
  type:        "n."
},
{
  word:        "science",
  translation: "nachna",
  type:        "n."
},
{
  word:        "scissors",
  translation: "pashren",
  type:        "n."
},
{
  word:        "screw",
  translation: "bhoot",
  type:        "v."
},
{
  word:        "scythe, related to",
  translation: "mon",
  type:        "adj."
},
{
  word:        "Scythes of Devastation",
  translation: "Kno’mon (a Trolloc band)",
  type:        "n."
},
{
  word:        "Scythes of War",
  translation: "Dhai’mon (a Trolloc band)",
  type:        "n."
},
{
  word:        "seat",
  translation: "banta",
  type:        "n."
},
{
  word:        "second",
  translation: "dvoyn",
  type:        "n. & adj."
},
{
  word:        "secret",
  translation: "tongel",
  type:        "n. & adj."
},
{
  word:        "secretary",
  translation: "odik",
  type:        "n."
},
{
  word:        "see",
  translation: "darshi",
  type:        "v."
},
{
  word:        "seed",
  translation: "vetan",
  type:        "n."
},
{
  word:        "seeker/seekers",
  translation: "mahdi/ mahdi’in (used for leader of Tuatha’an caravan)",
  type:        "n."
},
{
  word:        "seeking man of the stars",
  translation: "mah’alleinir (literal translation; name of Perrin’s Power-wrought hammer)",
  type:        "n."
},
{
  word:        "seem",
  translation: "umeil",
  type:        "v."
},
{
  word:        "selection",
  translation: "tipakati",
  type:        "n."
},
{
  word:        "self",
  translation: "baid",
  type:        "n. & adj."
},
{
  word:        "send",
  translation: "heinst",
  type:        "v."
},
{
  word:        "sense",
  translation: "vaakaja",
  type:        "n."
},
{
  word:        "separate",
  translation: "roscher",
  type:        "adj."
},
{
  word:        "serious",
  translation: "darm",
  type:        "adj."
},
{
  word:        "servant(s)",
  translation: "sedai",
  type:        "n."
},
{
  word:        "Servants of All",
  translation: "Aes Sedai",
  type:        "n."
},
{
  word:        "seven",
  translation: "sukyat (material objects); sukye (immaterial things)",
  type:        "adj."
},
{
  word:        "sex",
  translation: "sterpan",
  type:        "n."
},
{
  word:        "shade",
  translation: "dhamel",
  type:        "n."
},
{
  word:        "shadow",
  translation: "shadar",
  type:        "n."
},
{
  word:        "Shadow, Cutter (or Slicer) of the Shadow",
  translation: "Shadar Nor (name given to Latra Posae)",
  type:        "n."
},
{
  word:        "Shadow’s Waiting/Where the Shadow Waits",
  translation: "Shadar Logoth (name of the city Aridhol that became tainted with evil)",
  type:        "n."
},
{
  word:        "shake",
  translation: "raqit",
  type:        "v."
},
{
  word:        "shame",
  translation: "mikra",
  type:        "n."
},
{
  word:        "sharp",
  translation: "xelt",
  type:        "adj."
},
{
  word:        "she",
  translation: "sar",
  type:        "pron."
},
{
  word:        "she/the woman who is dedicated",
  translation: "shaiel (Tigraine’s Aiel name)",
  type:        "n."
},
{
  word:        "sheep",
  translation: "varkol",
  type:        "n."
},
{
  word:        "shelf",
  translation: "polov",
  type:        "n."
},
{
  word:        "shield(s)",
  translation: "aethan",
  type:        "n."
},
{
  word:        "ship",
  translation: "wakaput",
  type:        "n."
},
{
  word:        "shirt",
  translation: "galamok",
  type:        "n."
},
{
  word:        "shock",
  translation: "vream",
  type:        "n."
},
{
  word:        "shoe",
  translation: "kesool",
  type:        "n."
},
{
  word:        "short",
  translation: "toopan",
  type:        "adj."
},
{
  word:        "shut",
  translation: "chukhar",
  type:        "v. & adj."
},
{
  word:        "sick",
  translation: "bokhen",
  type:        "adj."
},
{
  word:        "side",
  translation: "xeust",
  type:        "n."
},
{
  word:        "sign",
  translation: "wishti",
  type:        "n."
},
{
  word:        "silk",
  translation: "qamir",
  type:        "n."
},
{
  word:        "silver",
  translation: "torian",
  type:        "n. & adj."
},
{
  word:        "simple",
  translation: "wadlian",
  type:        "adj."
},
{
  word:        "Sires of Agony",
  translation: "Dha’vol (a Trolloc band)",
  type:        "n."
},
{
  word:        "sister",
  translation: "dar",
  type:        "n."
},
{
  word:        "six",
  translation: "panyat (material objects); panye (immaterial things)",
  type:        "adj."
},
{
  word:        "size",
  translation: "katien",
  type:        "n."
},
{
  word:        "skin",
  translation: "timari",
  type:        "n."
},
{
  word:        "skinny",
  translation: "poldar",
  type:        "adj."
},
{
  word:        "skirt",
  translation: "caili",
  type:        "n."
},
{
  word:        "sky",
  translation: "oghri",
  type:        "n."
},
{
  word:        "slave",
  translation: "da’covale (Seanchan term; preferred form is covale, for property)",
  type:        "n."
},
{
  word:        "sleep",
  translation: "houma",
  type:        "v."
},
{
  word:        "slicer",
  translation: "nor",
  type:        "n."
},
{
  word:        "slicer of the living",
  translation: "aginor (a Forsaken)",
  type:        "n."
},
{
  word:        "slip",
  translation: "uttat",
  type:        "v."
},
{
  word:        "slope",
  translation: "ronagh",
  type:        "n."
},
{
  word:        "slow",
  translation: "balad",
  type:        "adj."
},
{
  word:        "small",
  translation: "chinti",
  type:        "adj."
},
{
  word:        "smash",
  translation: "uglat",
  type:        "v."
},
{
  word:        "smell",
  translation: "nais",
  type:        "v."
},
{
  word:        "smile",
  translation: "attik",
  type:        "n. & v."
},
{
  word:        "smoke",
  translation: "ghuni",
  type:        "n. & v."
},
{
  word:        "smooth",
  translation: "heesh",
  type:        "adj."
},
{
  word:        "snake",
  translation: "nagaru",
  type:        "n."
},
{
  word:        "snake, type of",
  translation: "coreer (from Age of Legends)",
  type:        "n."
},
{
  word:        "sneeze",
  translation: "chitzi",
  type:        "v."
},
{
  word:        "snow",
  translation: "yazpa",
  type:        "n. & v."
},
{
  word:        "so",
  translation: "moro",
  type:        "adv. & conj."
},
{
  word:        "soap",
  translation: "svayor",
  type:        "n."
},
{
  word:        "society",
  translation: "panjami",
  type:        "n."
},
{
  word:        "sock",
  translation: "mustiel",
  type:        "n."
},
{
  word:        "soft",
  translation: "zaleen",
  type:        "adj."
},
{
  word:        "soil",
  translation: "feros",
  type:        "n."
},
{
  word:        "soldier(s)",
  translation: "drin",
  type:        "n."
},
{
  word:        "solid",
  translation: "garan",
  type:        "adj."
},
{
  word:        "some",
  translation: "odi",
  type:        "pron. & adj."
},
{
  word:        "son",
  translation: "alep",
  type:        "n."
},
{
  word:        "song",
  translation: "dena",
  type:        "n."
},
{
  word:        "sort",
  translation: "vidnu",
  type:        "v."
},
{
  word:        "soul, the",
  translation: "ghoba",
  type:        "n."
},
{
  word:        "Soulless",
  translation: "Al’ghol (name of Trolloc band); gholam (a Shadowspawn)",
  type:        "n."
},
{
  word:        "sound",
  translation: "diy",
  type:        "v."
},
{
  word:        "sounder",
  translation: "diynen (one who produces a sound)",
  type:        "n."
},
{
  word:        "soup",
  translation: "chanda",
  type:        "n."
},
{
  word:        "south",
  translation: "djanzei",
  type:        "n., adj. & adv."
},
{
  word:        "sovereign",
  translation: "tan",
  type:        "n. & adj."
},
{
  word:        "space",
  translation: "hirato",
  type:        "n."
},
{
  word:        "spade",
  translation: "obidum",
  type:        "n."
},
{
  word:        "spawn",
  translation: "bajad",
  type:        "n."
},
{
  word:        "speak",
  translation: "nosane",
  type:        "v."
},
{
  word:        "speaker",
  translation: "feia",
  type:        "n."
},
{
  word:        "Speaker of the Truth (Seanchan)",
  translation: "Soe’feia",
  type:        "n."
},
{
  word:        "spear",
  translation: "dareis (Aiel); plural is darei",
  type:        "n."
},
{
  word:        "spear fighters",
  translation: "algai’d’siswai (an Aiel term)",
  type:        "n."
},
{
  word:        "spear(s) of the dragon",
  translation: "siswai’aman (term used by the Aiel)",
  type:        "n."
},
{
  word:        "special",
  translation: "bolar",
  type:        "adj."
},
{
  word:        "spider",
  translation: "moghedien (a particular breed: small and poisonous; name of a Forsaken)",
  type:        "n."
},
{
  word:        "sponge",
  translation: "otiel",
  type:        "n."
},
{
  word:        "spoon",
  translation: "wastin",
  type:        "n."
},
{
  word:        "spring",
  translation: "vesna",
  type:        "n."
},
{
  word:        "square",
  translation: "ploushin",
  type:        "n. & adj."
},
{
  word:        "stage",
  translation: "leffal",
  type:        "n."
},
{
  word:        "stamp",
  translation: "tamu",
  type:        "n."
},
{
  word:        "star(s)",
  translation: "onir",
  type:        "n."
},
{
  word:        "start",
  translation: "pochivat",
  type:        "v."
},
{
  word:        "statement",
  translation: "furthadin",
  type:        "n."
},
{
  word:        "station",
  translation: "vaeku",
  type:        "v."
},
{
  word:        "steady",
  translation: "taer",
  type:        "adj."
},
{
  word:        "steel",
  translation: "kesan",
  type:        "n. & adj."
},
{
  word:        "stem",
  translation: "stobur",
  type:        "n."
},
{
  word:        "step",
  translation: "nieya",
  type:        "v."
},
{
  word:        "sticky",
  translation: "baichan",
  type:        "adj."
},
{
  word:        "stiff",
  translation: "tolin",
  type:        "adj."
},
{
  word:        "still",
  translation: "bift",
  type:        "adj."
},
{
  word:        "stitch",
  translation: "zinik",
  type:        "n."
},
{
  word:        "stocking",
  translation: "pinchota",
  type:        "n."
},
{
  word:        "stomach",
  translation: "bloobh",
  type:        "n."
},
{
  word:        "stone",
  translation: "andillar; suffix form is –dillar; taal (n. & adj.)",
  type:        "n."
},
{
  word:        "stone, of",
  translation: "m’taal",
  type:        "adj."
},
{
  word:        "Stone Dogs",
  translation: "Shae’en M’taal (Aiel warrior society)",
  type:        "n."
},
{
  word:        "stop",
  translation: "desta",
  type:        "v."
},
{
  word:        "store",
  translation: "kramtor",
  type:        "n."
},
{
  word:        "storm",
  translation: "tsorovan (sometimes meaning a smaller storm)",
  type:        "n."
},
{
  word:        "storm, winter",
  translation: "cemaros (specifically, a tempest from Sea of Storms)",
  type:        "n."
},
{
  word:        "Storm Leader",
  translation: "Tsorovan’m’hael (what the Asha’man Gedwyn called himself)",
  type:        "n."
},
{
  word:        "story",
  translation: "yatanel",
  type:        "n."
},
{
  word:        "strange",
  translation: "choutsin",
  type:        "adj."
},
{
  word:        "steam",
  translation: "smoog",
  type:        "n."
},
{
  word:        "street",
  translation: "ravad",
  type:        "n."
},
{
  word:        "stretch",
  translation: "gruget",
  type:        "v."
},
{
  word:        "strive",
  translation: "dai",
  type:        "n., v. & adj."
},
{
  word:        "stones",
  translation: "no’ri (modern name of ancient game from Moridin POV)",
  type:        "n."
},
{
  word:        "straight/straightforward",
  translation: "taer",
  type:        "adj."
},
{
  word:        "strong",
  translation: "frait",
  type:        "adj."
},
{
  word:        "Strong Wind",
  translation: "Ahf’frait (a Trolloc band)",
  type:        "n."
},
{
  word:        "structure",
  translation: "houghan",
  type:        "n."
},
{
  word:        "struggle",
  translation: "dai",
  type:        "n., v. & adj."
},
{
  word:        "stubborn",
  translation: "seren",
  type:        "adj."
},
{
  word:        "stubborn daughter",
  translation: "serenla (name that Siuan gave to Min)",
  type:        "n."
},
{
  word:        "stubborn one",
  translation: "serenda (also name of the Amadician king’s palace)",
  type:        "n."
},
{
  word:        "substance",
  translation: "sulwed",
  type:        "n."
},
{
  word:        "such",
  translation: "prato",
  type:        "adj."
},
{
  word:        "sudden",
  translation: "rabdo",
  type:        "adj."
},
{
  word:        "sugar",
  translation: "medan",
  type:        "n."
},
{
  word:        "suggestion",
  translation: "breudon",
  type:        "n."
},
{
  word:        "summer",
  translation: "laido",
  type:        "n."
},
{
  word:        "summons",
  translation: "concion",
  type:        "n."
},
{
  word:        "sun",
  translation: "ellis",
  type:        "n."
},
{
  word:        "support",
  translation: "chenal",
  type:        "n."
},
{
  word:        "surprise",
  translation: "ubriva",
  type:        "n."
},
{
  word:        "swallow",
  translation: "akein (bird)",
  type:        "n."
},
{
  word:        "swear",
  translation: "begrat",
  type:        "v."
},
{
  word:        "sweat tent scraping stick",
  translation: "staera (made of copper, used by Aiel)",
  type:        "n."
},
{
  word:        "sweet",
  translation: "chalin",
  type:        "adj."
},
{
  word:        "sweet girl",
  translation: "chalinda (Old Tongue name give to Min by Siuan)",
  type:        "n."
},
{
  word:        "swim",
  translation: "marna",
  type:        "v."
},
{
  word:        "sword",
  translation: "manshima",
  type:        "n."
},
{
  word:        "sword, my own",
  translation: "manshimaya",
  type:        "n."
},
{
  word:        "sword, related to",
  translation: "man",
  type:        "adj."
},
{
  word:        "Sword that is not a Sword, The",
  translation: "Callandor",
  type:        "n."
},
{
  word:        "sworn",
  translation: "begratanae",
  type:        "adj."
},
{
  word:        "system",
  translation: "trefon",
  type:        "n."
},
{
  word:        "table",
  translation: "ramay",
  type:        "n."
},
{
  word:        "tail",
  translation: "kovist",
  type:        "n."
},
{
  word:        "take",
  translation: "harben",
  type:        "v."
},
{
  word:        "Taker of Souls",
  translation: "Gho’hlem (a Trolloc band)",
  type:        "n."
},
{
  word:        "talk",
  translation: "bolga",
  type:        "v."
},
{
  word:        "tall",
  translation: "aird",
  type:        "adj."
},
{
  word:        "talon",
  translation: "cha",
  type:        "n."
},
{
  word:        "taste",
  translation: "spashoi",
  type:        "n."
},
{
  word:        "tax",
  translation: "kaarto",
  type:        "n."
},
{
  word:        "teacher",
  translation: "saana",
  type:        "n."
},
{
  word:        "teacher of lessons",
  translation: "mesaana (name of a Forsaken)",
  type:        "n."
},
{
  word:        "teaching",
  translation: "saantar",
  type:        "n."
},
{
  word:        "tempest",
  translation: "cemaros (specifically, a winter storm from Sea of Storms)",
  type:        "n."
},
{
  word:        "ten",
  translation: "desyat (material objects); desye (immaterial things)",
  type:        "adj."
},
{
  word:        "tendency",
  translation: "pranent",
  type:        "n."
},
{
  word:        "terror",
  translation: "dhjin",
  type:        "n."
},
{
  word:        "test",
  translation: "profel",
  type:        "v."
},
{
  word:        "than",
  translation: "yak",
  type:        "conj."
},
{
  word:        "that",
  translation: "wot",
  type:        "pron. & adj."
},
{
  word:        "themselves",
  translation: "se",
  type:        "pron."
},
{
  word:        "then",
  translation: "patra",
  type:        "n. & adv."
},
{
  word:        "theory",
  translation: "dantor",
  type:        "n."
},
{
  word:        "there",
  translation: "koja",
  type:        "n. & adv."
},
{
  word:        "thick",
  translation: "doorn",
  type:        "adj."
},
{
  word:        "thin",
  translation: "simp",
  type:        "adj."
},
{
  word:        "thing",
  translation: "so",
  type:        "n."
},
{
  word:        "this",
  translation: "iqet",
  type:        "pron., adj. & adv."
},
{
  word:        "those who cause change or are tied to change",
  translation: "veren",
  type:        "n."
},
{
  word:        "Those Who Cause Terror",
  translation: "Dhjin’nen (a Trolloc band)",
  type:        "n."
},
{
  word:        "Those Who Come Before",
  translation: "Hailene (Seanchan term)",
  type:        "n."
},
{
  word:        "Those Who Come Home",
  translation: "Rhyagelle (Seanchan term)",
  type:        "n."
},
{
  word:        "those who exemplify something",
  translation: "sheen",
  type:        "n."
},
{
  word:        "those who must be leashed",
  translation: "marath’damane (Seanchan word)",
  type:        "n."
},
{
  word:        "though",
  translation: "zazit",
  type:        "conj."
},
{
  word:        "thought",
  translation: "nardes",
  type:        "n."
},
{
  word:        "thousand, one",
  translation: "tuhat",
  type:        "adj."
},
{
  word:        "thousands",
  translation: "tuhat (as in, tre’tuhat, meaning 3,000)",
  type:        "suffix"
},
{
  word:        "thread",
  translation: "gadhat",
  type:        "n."
},
{
  word:        "three",
  translation: "treyat (material objects); treye (immaterial things)",
  type:        "adj."
},
{
  word:        "throat",
  translation: "pyast",
  type:        "n."
},
{
  word:        "through/through this or it",
  translation: "nesodhin",
  type:        "prep."
},
{
  word:        "thumb",
  translation: "ibalets",
  type:        "n."
},
{
  word:        "thunder",
  translation: "sha’mad",
  type:        "n."
},
{
  word:        "Thunder Walkers",
  translation: "Sha’mad Conde (Aiel warrior society)",
  type:        "n."
},
{
  word:        "thus",
  translation: "ghiro",
  type:        "adv."
},
{
  word:        "ticket",
  translation: "kippat",
  type:        "n."
},
{
  word:        "tight",
  translation: "kritam",
  type:        "adj."
},
{
  word:        "till",
  translation: "hosiya",
  type:        "v."
},
{
  word:        "time",
  translation: "sag",
  type:        "n."
},
{
  word:        "time, it is",
  translation: "sagain",
  type:        "n."
},
{
  word:        "tin",
  translation: "olcam",
  type:        "n."
},
{
  word:        "tired",
  translation: "claddin",
  type:        "adj."
},
{
  word:        "title, a",
  translation: "Nae’blis (Shaitan’s first lieutenant)",
  type:        "n."
},
{
  word:        "to",
  translation: "ti",
  type:        "prep."
},
{
  word:        "to/to the",
  translation: "tia",
  type:        "prep."
},
{
  word:        "toe",
  translation: "ocarn",
  type:        "n."
},
{
  word:        "together",
  translation: "perant",
  type:        "adv."
},
{
  word:        "tomato",
  translation: "t’mat (from the Aiel Waste)",
  type:        "n."
},
{
  word:        "tomb",
  translation: "moridin (name of a Forsaken, the word referring to death)",
  type:        "n."
},
{
  word:        "tomorrow",
  translation: "welakai",
  type:        "n. & adv."
},
{
  word:        "tongue",
  translation: "diutic",
  type:        "n."
},
{
  word:        "tooth",
  translation: "jabro",
  type:        "n."
},
{
  word:        "top",
  translation: "otou",
  type:        "n. & adj."
},
{
  word:        "touch",
  translation: "torkat",
  type:        "v."
},
{
  word:        "tower",
  translation: "tar",
  type:        "n."
},
{
  word:        "tower of man",
  translation: "tarasin (name of palace in Ebou Dar)",
  type:        "n."
},
{
  word:        "Tower that Guards",
  translation: "Tar Valon",
  type:        "n."
},
{
  word:        "town",
  translation: "lagien",
  type:        "n."
},
{
  word:        "trade",
  translation: "masnad",
  type:        "n."
},
{
  word:        "train",
  translation: "cantheal",
  type:        "n."
},
{
  word:        "transparent",
  translation: "telio",
  type:        "adj."
},
{
  word:        "transport",
  translation: "ethaantar",
  type:        "v."
},
{
  word:        "trap",
  translation: "cour",
  type:        "n."
},
{
  word:        "traveler",
  translation: "tuatha (one going from one place to another; can be a vagabond)",
  type:        "n."
},
{
  word:        "traveling box(es)",
  translation: "nar’baha (foolbox(es) used by Sammael)",
  type:        "n."
},
{
  word:        "Traveling People, the",
  translation: "Tuatha’an",
  type:        "n."
},
{
  word:        "tray",
  translation: "usont",
  type:        "n."
},
{
  word:        "treaty",
  translation: "dae’vin",
  type:        "n."
},
{
  word:        "tree(s)",
  translation: "avende",
  type:        "n."
},
{
  word:        "Tree of Life",
  translation: "Avendesora; Avendoraldera is an offshoot found outside the Waste",
  type:        "n."
},
{
  word:        "treebrother",
  translation: "t’ingshen (compound word used in addressing Ogier, meaning “to you—representing something most important (i.e., the tree)—in brotherhood”)",
  type:        "n."
},
{
  word:        "trick",
  translation: "risor",
  type:        "v."
},
{
  word:        "trouble",
  translation: "lishno",
  type:        "n."
},
{
  word:        "trousers",
  translation: "theini",
  type:        "n."
},
{
  word:        "true",
  translation: "jenn; tai (adj.; plural is “tain”)",
  type:        "adj. & adv."
},
{
  word:        "true blood",
  translation: "tai’shar (part of a greeting of honor used by the Borderlanders)",
  type:        "n."
},
{
  word:        "True Bloods",
  translation: "Tain Shari (Aiel warrior society)",
  type:        "n."
},
{
  word:        "true finder",
  translation: "jeade’en (Jain Farstrider’s and Rand’s horse)",
  type:        "n."
},
{
  word:        "truly",
  translation: "jenn",
  type:        "adj. & adv."
},
{
  word:        "truth",
  translation: "imsoen; soe",
  type:        "n."
},
{
  word:        "Truth Speaker (Seanchan)",
  translation: "Soe’feia",
  type:        "n."
},
{
  word:        "turn",
  translation: "drenni",
  type:        "v."
},
{
  word:        "twist",
  translation: "dred; pakita",
  type:        "v."
},
{
  word:        "two",
  translation: "dvoyat (material objects); dvoye (immaterial things)",
  type:        "adj."
},
{
  word:        "ultimate",
  translation: "tarmon",
  type:        "adj."
},
{
  word:        "uncle, little",
  translation: "magami (Amalisa’s private name for King Easar)",
  type:        "n."
},
{
  word:        "under",
  translation: "basho",
  type:        "prep., adj. & adv."
},
{
  word:        "unit",
  translation: "kozat",
  type:        "n."
},
{
  word:        "Unseen World, the",
  translation: "Tel’aran’rhiod (term used by Aes Sedai)",
  type:        "n."
},
{
  word:        "up",
  translation: "pad",
  type:        "adj., adv. & prep."
},
{
  word:        "use",
  translation: "gorista",
  type:        "v."
},
{
  word:        "veil, dust",
  translation: "shoufa (used by Aiel)",
  type:        "n."
},
{
  word:        "venom",
  translation: "ghar",
  type:        "n."
},
{
  word:        "verse",
  translation: "fenter",
  type:        "n."
},
{
  word:        "very",
  translation: "maani",
  type:        "adv."
},
{
  word:        "vessel",
  translation: "dal",
  type:        "n."
},
{
  word:        "vessel of pleasure",
  translation: "graendal (name of a Forsaken)",
  type:        "n."
},
{
  word:        "view",
  translation: "obrafad",
  type:        "n."
},
{
  word:        "violent",
  translation: "chekrut",
  type:        "adj."
},
{
  word:        "voice",
  translation: "tati",
  type:        "n."
},
{
  word:        "void, the",
  translation: "ko’di (a meditative state)",
  type:        "n."
},
{
  word:        "walk",
  translation: "conden",
  type:        "v."
},
{
  word:        "walker(s)",
  translation: "conde",
  type:        "n."
},
{
  word:        "wall",
  translation: "tinto",
  type:        "n."
},
{
  word:        "war-related",
  translation: "dhai",
  type:        "adj."
},
{
  word:        "warm(s)",
  translation: "domashita",
  type:        "v."
},
{
  word:        "wash",
  translation: "panati",
  type:        "v."
},
{
  word:        "waste",
  translation: "difrol",
  type:        "n."
},
{
  word:        "watch",
  translation: "vronne",
  type:        "v."
},
{
  word:        "watcher(s)",
  translation: "a’vron; vron",
  type:        "n."
},
{
  word:        "watchers of importance",
  translation: "ma’vron",
  type:        "n."
},
{
  word:        "Watchers over the Waves",
  translation: "Do Miere A’vron",
  type:        "n."
},
{
  word:        "water",
  translation: "duadhe",
  type:        "n."
},
{
  word:        "Water Seekers",
  translation: "Duadhe Mahdi’in (an Aiel warrior society)",
  type:        "n."
},
{
  word:        "waters of the mountain home",
  translation: "manetherendrelle",
  type:        "n."
},
{
  word:        "waves",
  translation: "miere",
  type:        "n."
},
{
  word:        "wax",
  translation: "ikaat",
  type:        "n."
},
{
  word:        "way",
  translation: "veshan",
  type:        "n."
},
{
  word:        "we",
  translation: "iro",
  type:        "pron."
},
{
  word:        "weak",
  translation: "komalin",
  type:        "adj."
},
{
  word:        "weather",
  translation: "odashi",
  type:        "n."
},
{
  word:        "web of destiny",
  translation: "ta’maral’ailen (Ogier term describing the ta’veren phenomenon)",
  type:        "n."
},
{
  word:        "week",
  translation: "hoptah",
  type:        "n."
},
{
  word:        "weight",
  translation: "runyat",
  type:        "n."
},
{
  word:        "welcome",
  translation: "calichniye",
  type:        "interjection"
},
{
  word:        "well",
  translation: "afwadh",
  type:        "n."
},
{
  word:        "west",
  translation: "koudam",
  type:        "n., adj. & adv."
},
{
  word:        "westwind",
  translation: "aldieb (the wind that brings spring rains)",
  type:        "n."
},
{
  word:        "wet",
  translation: "zoppen",
  type:        "adj."
},
{
  word:        "what",
  translation: "gavane",
  type:        "pron., adj. & adv."
},
{
  word:        "what was asked",
  translation: "devoriska",
  type:        "rel. pron."
},
{
  word:        "wheel",
  translation: "makitai",
  type:        "n."
},
{
  word:        "when",
  translation: "qen",
  type:        "adv. & conj."
},
{
  word:        "where",
  translation: "doko",
  type:        "pron. & adv."
},
{
  word:        "while",
  translation: "vaesht",
  type:        "n. & conj."
},
{
  word:        "whip",
  translation: "remath",
  type:        "n. & v."
},
{
  word:        "whip, energy",
  translation: "rema’kar (Age of Legends weapon)",
  type:        "n."
},
{
  word:        "whistle",
  translation: "pistit",
  type:        "n."
},
{
  word:        "whistle",
  translation: "pistita",
  type:        "v."
},
{
  word:        "white",
  translation: "safar",
  type:        "n. & adj."
},
{
  word:        "who",
  translation: "nak",
  type:        "pron."
},
{
  word:        "why",
  translation: "neisen",
  type:        "adv."
},
{
  word:        "wide",
  translation: "hawali",
  type:        "adj."
},
{
  word:        "will",
  translation: "zavilat",
  type:        "n."
},
{
  word:        "wind, as an air current",
  translation: "ahf; dieb",
  type:        "n."
},
{
  word:        "window",
  translation: "ovage",
  type:        "n."
},
{
  word:        "wine",
  translation: "ounadh",
  type:        "n."
},
{
  word:        "wing",
  translation: "stripo",
  type:        "n."
},
{
  word:        "winter",
  translation: "chiema",
  type:        "n."
},
{
  word:        "wire",
  translation: "gwiltor",
  type:        "n."
},
{
  word:        "wise",
  translation: "washdor",
  type:        "adj."
},
{
  word:        "with",
  translation: "vid",
  type:        "prep."
},
{
  word:        "without",
  translation: "mera",
  type:        "prep."
},
{
  word:        "without forgiveness",
  translation: "hessalam (name of a Forsaken)",
  type:        "n. & adj."
},
{
  word:        "wolf",
  translation: "vovok",
  type:        "n."
},
{
  word:        "woman",
  translation: "shai",
  type:        "n."
},
{
  word:        "woman, old",
  translation: "drova; possessive is drovja",
  type:        "n."
},
{
  word:        "wood",
  translation: "lavakh",
  type:        "n."
},
{
  word:        "wool",
  translation: "izaad",
  type:        "n."
},
{
  word:        "word",
  translation: "palatu",
  type:        "n."
},
{
  word:        "work(ing)",
  translation: "sor",
  type:        "n. & adj."
},
{
  word:        "working clothes",
  translation: "cadin’sor (worn by Aiel)",
  type:        "n."
},
{
  word:        "world, a",
  translation: "rhiod",
  type:        "n."
},
{
  word:        "World of Dreams",
  translation: "Tel’aran’rhiod (term used by Aes Sedai)",
  type:        "n."
},
{
  word:        "worm",
  translation: "juma",
  type:        "n."
},
{
  word:        "wormwood",
  translation: "ordeith (name taken by Padan Fain while with the Whitecloaks)",
  type:        "n."
},
{
  word:        "worth",
  translation: "carentin",
  type:        "n."
},
{
  word:        "wound",
  translation: "hanol",
  type:        "n."
},
{
  word:        "write",
  translation: "lorme",
  type:        "v."
},
{
  word:        "writing",
  translation: "vyavi",
  type:        "n."
},
{
  word:        "written, is",
  translation: "lormae",
  type:        "v."
},
{
  word:        "wrong",
  translation: "nasai",
  type:        "n., v. & adj."
},
{
  word:        "year",
  translation: "khamu",
  type:        "n."
},
{
  word:        "yellow",
  translation: "zafar",
  type:        "adj."
},
{
  word:        "yes",
  translation: "taak",
  type:        "adv."
},
{
  word:        "yesterday",
  translation: "betakai",
  type:        "n. & adv."
},
{
  word:        "you",
  translation: "asa",
  type:        "pron. & n."
},
{
  word:        "you give",
  translation: "ma",
  type:        "v."
},
{
  word:        "young/youth-related",
  translation: "thamel",
  type:        "adj."
},
{
  word:        "your",
  translation: "ninte (formal); ninto (informal)",
  type:        "poss. pron."
},
{
  word:        "your heart",
  translation: "ba’asa",
  type:        "n."
},
{
  word:        "Age of Legends board games",
  translation: "sha’rah (Moridin POV); tcheran (Moridin POV); zara (played by followers of the Dark One, the pieces of which are live humans)",
},
{
  word:        "Age of Legends constructs",
  translation: "Nym",
},
{
  word:        "Age of Legends creatures",
  translation: "cafar; cosa; darath; dornat; jegal (scaled)",
},
{
  word:        "Age of Legends duel",
  translation: "sha’je (mentioned by Semirhage)",
  type:        "in Qal"
},
{
  word:        "Age of Legends musical instruments",
  translation: "balfone; corea; obaen; shama",
},
{
  word:        "Age of Legends textile with emotion-linked color-changing properties",
  translation: "streith",
},
{
  word:        "Age of Legends vehicle",
  translation: "sofar (with steering planes; from Semirhage POV)",
},
{
  word:        "Aginor’s creations",
  translation: "jumara (Worms, found in the Blight); zomara(n) (zombie-like creatures used as servants",
},
{
  word:        "Aiel foods and beverages",
  translation: "kardon (green-skinned fruit from leafless spiny plant in Aiel Waste); oosquai (distilled spirit); t’mat; zemai",
},
{
  word:        "Aiel Waste plants",
  translation: "pecara (tree with pale, wrinkled fruit); segade (spiny leathery plant with white blossoms); algode",
},
{
  word:        "Ogier greeting, formal",
  translation: "choba (“to the humble one before you”); choshih (“to the -  -  - unworthy one before you”)",
},
{
  word:        "Ogier homeland",
  translation: "stedding",
  type:        "a place of sanctuary"
},
{
  word:        "prefix added to first name of Malkieri royalty",
  translation: "al (kings); el (queens)",
},
{
  word:        "prefix denoting importance",
  translation: "ma",
},
{
  word:        "prefix denoting the superlative",
  translation: "sa",
},
{
  word:        "prefix meaning ‘from’",
  translation: "n",
},
{
  word:        "prefix meaning ‘of’",
  translation: "m; n",
},
{
  word:        "prefix referring to an agent of action",
  translation: "de",
},
{
  word:        "prefix referring to beauty",
  translation: "am",
},
{
  word:        "prefix referring to a handler or controller",
  translation: "morat (used by Seanchan re exotic animals)",
},
{
  word:        "prefix referring to the heart",
  translation: "cue, cuen",
},
{
  word:        "prefix referring to limited or specific application",
  translation: "ter",
},
{
  word:        "prefix (adjectival) referring to power",
  translation: "sai",
},
{
  word:        "prefix suggesting urgency or compulsion",
  translation: "marath (Seanchan word)",
},
{
  word:        "Seanchan exotic animals",
  translation: "corlm; grolm; lopar; raken; to’raken; torm",
},
{
  word:        "Seanchan hereditary upper servants of the blood",
  translation: "so’jhin (freely translated as “a height among lowliness” or “both sky and valley”; “a thing of exaltation”)",
},
{
  word:        "suffix denoting brutes, beasts, monsters",
  translation: "ghael",
},
{
  word:        "suffix denoting importance",
  translation: "don; ing (utmost importance)",
},
{
  word:        "suffix denoting multiples of ten",
  translation: "shi (apostrophe before)",
},
{
  word:        "suffix denoting negation",
  translation: "de",
},
{
  word:        "suffix denoting passive voice",
  translation: "ae (used with verbs)",
},
{
  word:        "suffix denoting plural form",
  translation: "a; an; en; in; on",
},
{
  word:        "suffix denoting river or water(s) of",
  translation: "drelle",
},
{
  word:        "suffix denoting stone",
  translation: "illar",
},
{
  word:        "suffix denoting stone-like quality",
  translation: "andi",
},
{
  word:        "suffix indicating one who or that which does, or those who cause",
  translation: "nen (like adding ‘er’ to English verb)",
},
{
  word:        "suffix indicating past tense",
  translation: "ane",
},
{
  word:        "suffix indicating personal possession",
  translation: "ara",
  type:        "i.e., my"
},
{
  word:        "suffix indicating feminine",
  translation: "dar",
},
{
  word:        "suffix indicating a lack, being without",
  translation: "lam",
},
{
  word:        "suffix indicating masculine",
  translation: "din",
},
{
  word:        "suffix indicating numerical teens",
  translation: "pi",
},
{
  word:        "suffix meaning ‘blue’",
  translation: "era",
},
{
  word:        "suffix meaning ‘many’",
  translation: "es",
},
{
  word:        "suffix meaning ‘my own’",
  translation: "ya",
},
{
  word:        "suffix meaning ‘of’ or ‘issued from’",
  translation: "ja",
},
{
  word:        "suffix meaning ‘punishment through the nervous system’",
  translation: "kar",
},
{
  word:        "suffix meaning ‘that which was’",
  translation: "iska",
},
{
  word:        "suffix meaning ‘those who harvest’",
  translation: "hlin",
},
{
  word:        "suffix meaning ‘those who take’",
  translation: "hlem",
},
{
  word:        "suffix meaning ‘true’",
  translation: "en",
},
{
  word:        "tree from Age of Legends, a construct",
  translation: "chora (a beneficent tree)",
},
{
  word:        "True Power artifact",
  translation: "saa (tiny black fleck that moves across Forsaken’s eyes when the True Power is accessed, increasing with more use of the TP)",
},
{
  word:        "unknown word",
  translation: "piesa (Leya’s horse)",
},
{
  word:        "wheel used in gambling",
  translation: "chinje (roulette-like)",
}
];

function search() {
  var search_word_input = document.getElementById("search_word");
  var search_term = search_word_input.value.toLowerCase();
  var search_results = [];
  var number_of_entries_text = `There are currently ${words.length} entries in the dictionary`;
  if (search_term !== '') {
    search_results = words.filter(entry => entry.word.includes(search_term) || entry.translation.includes(search_term));
    number_of_entries_text = `Found ${search_results.length} entries matching the term '${search_term}' in the dictionary`;
  } else {
    search_results = words;
  }
  document.getElementById("result").innerHTML = printSearchResults(search_results);
  document.getElementById("number_of_entries").innerHTML = number_of_entries_text;
}

function printSearchResults(results) {
  if (results.length == 0) {
    return '';
  }
  var html = `
    <table class="table is-striped is-fullwidth">
      <thead>
        <tr>
          <th>Word</th>
          <th>Type</th>
          <th>Translation</th>
        </tr>
      </thead>
      <tbody>
  `;
  var results_length = results.length;
  for (i = 0; i < results_length; i++) {
    var word = results[i].word;
    var type = results[i].type || '';
    var translation = results[i].translation || '';

    html += `
    <tr>
      <td>${word}</td>
      <td>${type}</td>
      <td>${translation}</td>
    </tr>
    `;

  }
  html += `
      </tbody>
    </table>
  `;

  return html;
}

window.onload = function () {
  var search_word_input = document.getElementById("search_word");
  search_word_input.onkeyup = search;
  search_word_input.onblur = function() { search_word_input.focus(); };
  search_word_input.focus();
  search();
}
